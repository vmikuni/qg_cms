pyenv install anaconda3-2018.12
mkdir /work/$USER/.pyenv.ext
cd ~/.pyenv/versions/
#This can take a while since anaconda rather large
mv anaconda3-2018.12 /work/$USER/.pyenv.ext/
## wait....
ln -s /work/$USER/.pyenv.ext/anaconda3-2018.12/ anaconda3-2018.12
pyenv virtualenv anaconda3-2018.12 QGGPU
source activate QGGPU
# Requires input - Downgrade python <= 3.7.1 in order to run on T3 
conda install python=3.6.8 # 3.7.1
# Requires input
conda install keras tensorflow-gpu==1.14.0 scikit-learn 
conda install -c conda-forge xrootd
pip install matplotlib pandas pytest pytest-mock pytest-cov tables ConfigParser pylint scikit-learn pydot coloredlogs coffea[parsl]

pyenv virtualenv anaconda3-2018.12 QGCPU
source activate QGCPU
conda install python=3.6.8
conda install -c conda-forge xrootd
pip install keras tensorflow==1.14.0 scikit-learn 
pip install matplotlib pandas pytest pytest-mock pytest-cov tables ConfigParser pylint scikit-learn pydot coloredlogs coffea[parsl]
pyenv local QGCPU

if [ ! -d "$HOME/bin" ]
then
    echo "Creating bin folder under the home area."
    mkdir $HOME/bin
fi

export PARSLINSTALL=$(python -c "import parsl; print(parsl.__path__[0])")
ln -s $PARSLINSTALL/executors/high_throughput/process_worker_pool.py $HOME/bin
chmod +x $HOME/bin/process_worker_pool.py

