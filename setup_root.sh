#!/usr/bin/env bash                                                                                                                                                                                       
export COFFEAHOME=$PWD
export COFFEADATA=$PWD/data
export OMP_NUM_THREADS=1

source deactivate
source /cvmfs/sft.cern.ch/lcg/views/LCG_96python3/x86_64-centos7-gcc8-opt/setup.sh #for grid control 
