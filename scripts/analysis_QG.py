from coffea import hist
import uproot
from coffea.util import awkward
from coffea.analysis_objects import JaggedCandidateArray
import coffea.processor as processor
from coffea.processor import run_parsl_job
from coffea.processor.parsl.parsl_executor import parsl_executor
from coffea.lookup_tools import extractor
from coffea.lumi_tools import LumiMask
import numpy as np
import numba as nb
import matplotlib.pyplot as plt
import utils
import sys
import re
import h5py as h5
from optparse import OptionParser
import parsl
import os
from parsl.configs.local_threads import config
from parsl.providers import LocalProvider,SlurmProvider
from parsl.channels import LocalChannel,SSHChannel
from parsl.config import Config
from parsl.executors import HighThroughputExecutor
from parsl.launchers import SrunLauncher
from parsl.addresses import address_by_hostname



# This is a helper function which adds up the mass of two 4-vectors based on their components.
@nb.njit()
def massAddition(l1_px, l1_py, l1_pz, l1_E,
                 l2_px, l2_py, l2_pz, l2_E):
    return np.sqrt((l1_E + l2_E)**2 - 
                  ((l1_px + l2_px)**2 + 
                   (l1_py + l2_py)**2 + 
                   (l1_pz + l2_pz)**2))

# This is a helper function which determines how far the mass of a pair of leptons is from the Z mass.
@nb.njit()
def massFromZ(l1_px, l1_py, l1_pz, l1_E,
              l2_px, l2_py, l2_pz, l2_E):
    mass_Z = 91
    mass_ll = massAddition(l1_px, l1_py, l1_pz, l1_E,
                           l2_px, l2_py, l2_pz, l2_E)
    return np.abs(mass_ll - mass_Z)

# This is a helper function which adds up the transverse momentum of two leptons from their x and y components.
@nb.njit()
def ptAddition(l1_px, l1_py, l2_px, l2_py):
    return np.sqrt((l1_px + l2_px)**2 + (l1_py + l2_py)**2)

# This is a helper function which gets all pairs of values of an input array.
@nb.njit()
def combinations(array):
    combos = [(0, 0)]
    for i in array:
        for j in array:
            if ((i, j) not in combos) and ((j, i) not in combos) and (j != i):
                combos.append((i, j))
    return combos[1:]


def Writeh5(output, name,folder):
    if not os.path.exists(folder):
        os.makedirs(folder)    
    with h5.File(os.path.join(folder,'QG_{0}.h5'.format(name)), "w") as fh5:
        dset = fh5.create_dataset("data", data=output['data'])
        dset = fh5.create_dataset("pid", data=output['pid'])
        dset = fh5.create_dataset("global", data=output['global'])
        # dset = fh5.create_dataset("QGL", data=output['QGL'])
        # dset = fh5.create_dataset("btag", data=output['btag'])



def GetInvMass(electrons,muons):
    '''dilepton Invariant mass from e-mu channel'''
    px = electrons.p4.x + muons.p4.x
    py = electrons.p4.y + muons.p4.y
    pz = electrons.p4.z + muons.p4.z
    E = electrons.p4.energy + muons.p4.energy
    return np.sqrt(np.abs(E**2-px**2-py**2-pz**2))


class Channel():
    def __init__(self, name):
        self.name = name
        self.sel=None
        self.jets= None
        self.pfs=None
        self.pfs_pid=None
        self.glob = None
        self.QGL = None
        self.btag = None
        self.jet = None
        self.pid = None
        
    def apply_sel(self):
        self.pfs=self.pfs[self.sel]
        self.jets=self.jets[self.sel]
        #self.glob = self.glob[self.sel]

    def apply_jet_sel(self,isQuark=False):
        jet_matched_pfs = self.pfs[self.pfs.jetIdx>=0]

        #print(gen_electrons.pdgId)
        #Cuts on jets
        pfs_mask = (self.jets[jet_matched_pfs.jetIdx].pt>=200) & (np.abs(self.jets[jet_matched_pfs.jetIdx].eta)<=2.4) 
        #& (self.jets[jet_matched_pfs.jetIdx].jet_id==6)
        if isQuark:
            #pfs_mask = (pfs_mask) & (self.jets[jet_matched_pfs.jetIdx].partonFlavour>0) & (self.jets[jet_matched_pfs.jetIdx].partonFlavour<=3)
            pfs_mask = (pfs_mask) & (self.jets[jet_matched_pfs.jetIdx].partonFlavour>0) & (self.jets[jet_matched_pfs.jetIdx].partonFlavour==3)
        else:
            #pfs_mask = (pfs_mask) & (self.jets[jet_matched_pfs.jetIdx].partonFlavour==21)
            pfs_mask = (pfs_mask) & (self.jets[jet_matched_pfs.jetIdx].partonFlavour==1)
        self.pfs = self.pfs[pfs_mask]
        
        
    def datah5(self):         
        data = np.zeros((self.pfs.size,opt.points,13))
        nparts = self.pfs.counts        
        truncate = self.pfs[nparts>=opt.points]
        pad = self.pfs[nparts<opt.points]
        pad.dr = self.pfs.dr[nparts<opt.points] 
        truncate.dr = self.pfs.dr[nparts>=opt.points]
        jet_truncate = self.jet[nparts>=opt.points]
        jet_pad = self.jet[nparts<opt.points]


        if len(data[nparts>=opt.points]) >0:
            data[nparts>=opt.points,:,0]+=np.array((jet_truncate.eta[:,:opt.points] - truncate.eta[:,:opt.points]).tolist())
            data[nparts>=opt.points,:,1]+=np.array((jet_truncate.phi[:,:opt.points] - truncate.phi[:,:opt.points]).tolist())
            data[nparts>=opt.points,:,2]+=np.log(np.array(truncate.pt[:,:opt.points].tolist()))
            data[nparts>=opt.points,:,3]+=np.log(np.array(truncate.p4.energy[:,:opt.points].tolist()))
            data[nparts>=opt.points,:,4]+=np.log(np.array((truncate.pt[:,:opt.points]/jet_truncate.pt[:,:opt.points]).tolist()))
            data[nparts>=opt.points,:,5]+=np.log(np.array((truncate.p4.energy[:,:opt.points]/jet_truncate.p4.energy[:,:opt.points]).tolist()))
            data[nparts>=opt.points,:,6]+=np.array(truncate.dr[:,:opt.points].tolist())
            data[nparts>=opt.points,:,7]+=np.array(truncate.charge[:,:opt.points].tolist())
            data[nparts>=opt.points,:,8]+=np.array((np.abs(truncate.pdgId[:,:opt.points])==11).tolist())
            data[nparts>=opt.points,:,9]+=np.array((np.abs(truncate.pdgId[:,:opt.points])==13).tolist())
            data[nparts>=opt.points,:,10]+=np.array((np.abs(truncate.pdgId[:,:opt.points])==211).tolist())
            data[nparts>=opt.points,:,11]+=np.array((np.abs(truncate.pdgId[:,:opt.points])==130).tolist())
            data[nparts>=opt.points,:,12]+=np.array((np.abs(truncate.pdgId[:,:opt.points])==22).tolist())


            
        if len(data[nparts<opt.points]) >0:            
            data[nparts<opt.points,:,0] = ZeroPad(jet_pad.eta -pad.eta,data[nparts<opt.points,:,0])
            data[nparts<opt.points,:,1] = ZeroPad(jet_pad.phi - pad.phi,data[nparts<opt.points,:,1])
            data[nparts<opt.points,:,2] = ZeroPad(np.log(pad.pt),data[nparts<opt.points,:,2]) 
            data[nparts<opt.points,:,3] = ZeroPad(np.log(pad.p4.energy),data[nparts<opt.points,:,3]) 
            data[nparts<opt.points,:,4] = ZeroPad(np.log(pad.pt/jet_pad.pt),data[nparts<opt.points,:,4]) 
            data[nparts<opt.points,:,5] = ZeroPad(np.log(pad.p4.energy/jet_pad.p4.energy),data[nparts<opt.points,:,5]) 
            data[nparts<opt.points,:,6] = ZeroPad(pad.dr,data[nparts<opt.points,:,6])
            data[nparts<opt.points,:,7] = ZeroPad(pad.charge,data[nparts<opt.points,:,7])
            data[nparts<opt.points,:,8] = ZeroPad(np.abs(pad.pdgId)==11,data[nparts<opt.points,:,8])
            data[nparts<opt.points,:,9] = ZeroPad(np.abs(pad.pdgId)==13,data[nparts<opt.points,:,9])
            data[nparts<opt.points,:,10] = ZeroPad(np.abs(pad.pdgId)==211,data[nparts<opt.points,:,10])
            data[nparts<opt.points,:,11] = ZeroPad(np.abs(pad.pdgId)==130,data[nparts<opt.points,:,11])
            data[nparts<opt.points,:,12] = ZeroPad(np.abs(pad.pdgId)==22,data[nparts<opt.points,:,12])


        

        return data.tolist()

    

class SignalProcessor(processor.ProcessorABC):

    def __init__(self):
        dataset_axis = hist.Cat("dataset", "")        
        nparts_axis = hist.Bin("nparts", "N$_{particles}$ ", 100, 0, 100)        
        pt_axis = hist.Bin("pt", "p_T", 50, 20, 100)        

        axis_list = {
            'nparts':nparts_axis, 
            'pt':pt_axis, 
        }

        self.channels = ['quarks','gluons']
        dict_accumulator = {}
        ML_accumulator = {
            'data': processor.list_accumulator(),
            'pid': processor.list_accumulator(),
            'global': processor.list_accumulator(),
            # 'QGL': processor.list_accumulator(),
            # 'btag': processor.list_accumulator(),
        }
        dict_accumulator['ML'] = processor.dict_accumulator(ML_accumulator)

        for axis in axis_list:
            dict_accumulator["{}".format(axis)] = hist.Hist("Events", dataset_axis, axis_list[axis])
            
        

        for axis in axis_list:
            for channel in self.channels:
                dict_accumulator["{}_{}".format(axis,channel)] = hist.Hist("Events", dataset_axis, axis_list[axis])        
               
        dict_accumulator['cutflow']= processor.defaultdict_accumulator(int)

        self._accumulator = processor.dict_accumulator( dict_accumulator )
        

    @property
    def accumulator(self):
        return self._accumulator
    
        
    def process(self, df):

        output = self.accumulator.identity()
        dataset = df["dataset"]
        
        Jets = JaggedCandidateArray.candidatesfromcounts(
            df['nJet'],            
            pt=df['Jet_pt'].content,
            rawf=df['Jet_rawFactor'].content,
            eta=df['Jet_eta'].content,
            phi=df['Jet_phi'].content,
            mass=df['Jet_mass'].content,
            qgl=df['Jet_qgl'].content,
            btag=df['Jet_btagDeepFlavB'].content,
            partonFlavour=df['Jet_partonFlavour'].content,
            nParts = df['Jet_nConstituents'].content,
            jet_id= df['Jet_jetId'].content,
            )
        

        PFCands = JaggedCandidateArray.candidatesfromcounts(
            df['nJetPFCands'],
            pt=df['JetPFCands_pt'].content,
            eta=df['JetPFCands_eta'].content,
            phi=df['JetPFCands_phi'].content,
            mass=df['JetPFCands_mass'].content,
            jetIdx=df['JetPFCands_jetIdx'].content,
            trkChi2=df['JetPFCands_trkChi2'].content,            
            charge=df['JetPFCands_charge'].content,                                    
            pdgId=df['JetPFCands_pdgId'].content,                                    
            )


        lumi = np.ones(Jets.size,dtype=np.bool_)

        # if 'data' not in dataset:            
        #     lumi = np.ones(electrons.size,dtype=np.bool_)
        # else:
        #     if 'COFFEADATA' not in os.environ:
        #         print("ERROR: Enviroment variables not set. Run setup.sh first!")
        #         sys.exit()
        #     json_path = os.path.join(os.environ['COFFEADATA'],'2016/json')

        #     run = df["run"]
        #     lumiblock = df["luminosityBlock"]
        #     lumi_mask = LumiMask('{}/Cert_271036-284044_13TeV_ReReco_07Aug2017_Collisions16_JSON.txt'.format(json_path))
        #     lumi = lumi_mask(run,lumiblock)
                    

            
        #input()
        # This keeps track of how many events there are, as well as how many of each object exist in this events.
        output['cutflow']['all events'] += Jets.size
        output['cutflow']['all PFcands'] += PFCands.counts.sum()
        output['cutflow']['all Jets'] += Jets.counts.sum()

        # PFCands = PFCands[
        #     (PFCands.pt > 0.5)  & 
        #     (np.abs(PFCands.eta)<2.4) & 
        #     #(PFCands.trkChi2 < 5)  & 
        #     (PFCands.mass > -1) 
            
        # ]
        
        
        # # # # # # # # # #
        # EVENT SELECTION #
        # # # # # # # # # #


        output['cutflow']['filtered PFCands'] += PFCands.counts.sum()
        quarks = Channel("quarks")
        gluons = Channel("gluons")
        for channel in [quarks,gluons]:
            channel.pfs = PFCands
            channel.jets = Jets
            channel.apply_jet_sel("quarks" in channel.name)
            channel.sel = (channel.pfs.counts >= 1)
            channel.apply_sel()
        output['cutflow']['Jet filtered quarks'] += quarks.pfs.counts.sum()
        output['cutflow']['Jet filtered gluons'] += gluons.pfs.counts.sum()
        #Picking the first one for simplicity
        for channel in [quarks,gluons]:
            #print(channel.name, channel.pfs.jetIdx[0],channel.jets.partonFlavour[0])
            channel.pfs = channel.pfs[
                channel.pfs.jetIdx[:] == channel.pfs.jetIdx[:,0] 
            ]
            output['cutflow']['First jet {}'.format(channel.name)] += channel.pfs.counts.sum()
            output['nparts_{}'.format(channel.name)].fill(dataset=dataset, nparts=channel.pfs.counts.flatten())
            channel.jet = channel.jets[channel.pfs.jetIdx]
            output['pt_{}'.format(channel.name)].fill(dataset=dataset, pt=channel.jet.pt[:,0].flatten())
            cross = channel.pfs['p4'].cross(channel.jet[:,:1]['p4'])
            channel.pfs.dr = cross.i0.delta_r(cross.i1)
            
            channel.glob = np.concatenate((
                np.expand_dims(np.log(channel.jet.pt[:,0]),axis=-1),
                np.expand_dims(channel.jet.eta[:,0],axis=-1),
            ),axis=1)
            # channel.QGL = channel.jet.qgl[:,0]
            # channel.btag = channel.jet.btag[:,0]

        quarks.pid = (quarks.jet.mass[:,0] > -1)
        gluons.pid = (gluons.jet.mass[:,0] < -1)
        #balance the number of training events
        nquarks = (quarks.jet.mass[:,0]>-1).sum()
        ngluons = (gluons.jet.mass[:,0]>-1).sum()
        nmin = min(nquarks,ngluons)
        print(nmin)
        

        if opt.save_h5:
            for channel in [quarks,gluons]:
                #print(channel.pid.tolist())
                output['ML']['data']+=channel.datah5()[:nmin]
                output['ML']['pid']+=channel.pid.tolist()[:nmin]
                output['ML']['global']+=channel.glob.tolist()[:nmin]
                # output['ML']['QGL']+=channel.QGL.tolist()
                # output['ML']['btag']+=channel.btag.tolist()

        

                
        
        return output

    def postprocess(self, accumulator):


        return accumulator

def ZeroPad(values,zeros):
    for ival, value in enumerate(values):    
        zeros[ival][:len(value)] += np.array(value)
    return zeros


def GetSamplename(first_file):
    sname = ''
    for string in first_file.split('/'):        
        if '13TeV' in string or 'Run' in string:
            if 'Run' in string:
                sname = string.split('-')[0]
            else:
                sname = string
            break
    if sname == '': 
        print(first_file)
        print('ERROR: Cannot find the sample name')
        sys.exit()
    return utils.samples[sname]['name']




        

parser = OptionParser(usage="%prog [opt]  inputFiles")

parser.add_option("--samples",dest="samples", type="string", default='train', help="Specify which default samples to run [data/mc/all]. Default: data")
#parser.add_option("--plot",dest="plot", action="store_true", default=False, help="Make ratio plot at the end") #to be done
parser.add_option("-q", "--queue",  dest="queue", type="string", default="quick", help="Which queue to send the jobs. Default: %default")
parser.add_option("-p", "--nproc",  dest="nproc", type="long", default=1, help="Number of processes to use. Default: %default")
parser.add_option("--mem",  dest="mem", type="long", default=1000, help="Memory required in mb")
parser.add_option("--points", type="int", default=100, help="max number of particles to be used. Default %default")
parser.add_option("--cpu",  dest="cpu", type="long", default=4, help="Number of cpus to use. Default %default")
parser.add_option("--blocks",  dest="blocks", type="long", default=100, help="number of blocks. Default %default")
parser.add_option("--walltime",  dest="walltime", type="string", default="0:59:50", help="Max time for job run. Default %default")
parser.add_option("--chunk",  dest="chunk", type="long",  default=50000, help="Chunk size. Default %default")
parser.add_option("--maxchunk",  dest="maxchunk", type="long",  default=2e6, help="Maximum number of chunks. Default %default")
parser.add_option("--version",  dest="version", type="string",  default="", help="nametag to append to output file.")
parser.add_option("--no_parsl",  dest="no_parsl", action="store_true",  default=False, help="Run without parsl. Default: False")

parser.add_option("--save_h5",  action="store_true",  default=False, help="Save a .h5 file for ML training. Default: False")
parser.add_option("--h5folder", type="string", default="h5", help="Folder to store the h5 files. Default: %default")

(opt, args) = parser.parse_args()
samples = opt.samples


if len(args) < 1:
    if 'COFFEADATA' not in os.environ:
        print("ERROR: Enviroment variables not set. Run setup.sh first!")
        sys.exit()
      
    if samples == 'train': 
        file_name = os.path.join(os.environ['COFFEADATA'],'QG_300.txt')    
        fout_name = 'train_{}.root'.format(opt.version)
    elif samples == 'test': 
        file_name = os.path.join(os.environ['COFFEADATA'],'QG_test.txt')    
        fout_name = 'test_{}.root'.format(opt.version)
    elif samples == 'eval': 
        file_name = os.path.join(os.environ['COFFEADATA'],'QG_eval.txt')    
        fout_name = 'eval_{}.root'.format(opt.version)
    else:
        print("ERROR: You must specify what kind of dataset you want to run [--samples]")
    
    print('Loading sets from file')    
    files = []
    with open(os.path.join(file_name),'r') as fread:
        files = fread.readlines()
        files = [x.strip() for x in files] 
        idx = np.arange(len(files))
        np.random.shuffle(idx)
        files = np.array(files)[idx]
                    
else:
    files = args
    fout_name = 'out.root'

fileset={}
for f in files:
    name = GetSamplename(f)
    if name in fileset:
        fileset[name].append(f)
    else:
        fileset[name] = [f]

ismc = False
for sample in fileset:
    print('Files: {0}, sample name: {1}'.format(fileset[sample],sample))
    if 'data' not in sample:
        ismc = True

#if ismc: evaluator = GetEvaluator()


nproc = opt.nproc
sched_options = '''
##SBATCH --account=gpu_gres 
##SBATCH --partition=gpu    
#SBATCH --cpus-per-task=%d
#SBATCH --mem=%d
##SBATCH --exclude=t3wn56
''' % (opt.cpu,opt.mem) 

x509_proxy = '.x509up_u%s'%(os.getuid())
wrk_init = '''
export XRD_RUNFORKHANDLER=1
export X509_USER_PROXY=/t3home/%s/%s
'''%(os.environ['USER'],x509_proxy)



if opt.no_parsl:
    output = processor.run_uproot_job(fileset,
                                      treename='Events',
                                      processor_instance=SignalProcessor(),
                                      executor=processor.futures_executor,
                                      executor_args={'workers':nproc},
                                      maxchunks =opt.maxchunk,
                                      chunksize = opt.chunk
    )


else:
    print("id: ",os.getuid())
    print(wrk_init)
    #sched_opts = ''
    #wrk_init = ''


    slurm_htex = Config(
        executors=[
            HighThroughputExecutor(
                label="coffea_parsl_slurm",
                #worker_debug=True,
                address=address_by_hostname(),
                prefetch_capacity=0,  
                heartbeat_threshold=60,
                cores_per_worker=1,
                #cores_per_worker=opt.cpu,
                max_workers=opt.cpu,
                provider=SlurmProvider(
                    channel=LocalChannel(),
                    launcher=SrunLauncher(),
                    init_blocks=opt.blocks,
                    min_blocks = opt.blocks-20,                     
                    max_blocks=opt.blocks+20,
                    exclusive  = False,
                    parallelism=1,
                    nodes_per_block=1,
                    #cores_per_node = opt.cpu,
                    partition=opt.queue,
                    scheduler_options=sched_options,   # Enter scheduler_opt if needed
                    worker_init=wrk_init,         # Enter worker_init if needed
                    walltime=opt.walltime
                ),
            )
        ],
        initialize_logging=False,
        app_cache = True,
        retries=5,
        strategy='simple',
    )

    dfk = parsl.load(slurm_htex)


    output = processor.run_uproot_job(fileset,
                                      treename='Events',
                                      processor_instance=SignalProcessor(),
                                      executor=processor.parsl_executor,
                                      executor_args={'config':None, 'flatten': False},
                                      chunksize = opt.chunk,
                                      maxchunks =opt.maxchunk,
    )

    #np.save('test.npy', output)
for flow in output['cutflow']:
    print(flow, output['cutflow'][flow])
if opt.save_h5:
    Writeh5(output['ML'],opt.version,os.path.join(os.environ['COFFEAHOME'],opt.h5folder))
    

if not os.path.exists(os.path.join(os.environ['COFFEAHOME'],"pods")):
    os.makedirs(os.path.join(os.environ['COFFEAHOME'],"pods"))

fout = uproot.recreate(os.path.join(os.environ['COFFEAHOME'],"pods",fout_name))

for var in output:
    if var == 'cutflow' or var == 'ML':continue
    for dataset in fileset:
        if len(output[var][dataset].sum("dataset").values()) < 1: continue #Don't try to save an empty histogram
        fout["{0}_{1}".format(dataset,var)] = hist.export1d(output[var][dataset].sum("dataset"))
fout.close()

if not opt.no_parsl:
    parsl.dfk().cleanup()
    parsl.clear()
