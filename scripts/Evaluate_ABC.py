

class ABCModel():
    def __init__(self,model_name,batch,num_point=100,nfeat=13,nglob=2,params = [10,1,16,64,128,1,64,128,128,512,256,128]):
        import os, ast

        self.MODEL_PATH = os.path.join(os.environ['COFFEAHOME'],'logs',model_name)

        # MAIN SCRIPT
        self.NUM_POINT = num_point
        self.BATCH_SIZE = batch
        self.NFEATURES = nfeat
        self.NGLOB = nglob
        self.NUM_CATEGORIES = 4
        self.PARAMS = params

    def eval(self,data):
        '''Return the predictions given a feed_dic of the form:
        data = {
        'pointclouds_pl': batch_data,
        'labels_pl': batch_label,
        'global_pl':batch_global,
        }
        '''

        import sys
        import h5py
        import tensorflow as tf
        import numpy as np
        import os, ast
        BASE_DIR = os.path.join(os.environ['COFFEAHOME'])
        sys.path.append(os.path.join(os.path.dirname(BASE_DIR),'scripts'))
        sys.path.append(os.path.join(BASE_DIR,'scripts','models'))
        import provider
        import gapnet as MODEL
        



        with tf.Graph().as_default():
            with tf.device('/cpu:0'):
                pointclouds_pl,  labels_pl, global_pl = MODEL.placeholder_inputs(self.BATCH_SIZE, self.NUM_POINT,self.NFEATURES,num_glob=self.NGLOB)
          
                batch = tf.Variable(0, trainable=False)
                #norms = tf.placeholder(tf.float32,shape=(self.BATCH_SIZE))
                is_training_pl = tf.placeholder(tf.bool, shape=())
                pred,coef1,coef2 = MODEL.get_model(pointclouds_pl, is_training=is_training_pl,global_pl = global_pl,params=self.PARAMS,num_class=self.NUM_CATEGORIES)
                pred = tf.nn.softmax(pred)
            
            
                saver = tf.train.Saver()
          

    
            config = tf.ConfigProto()
            config.gpu_options.allow_growth = True
            config.allow_soft_placement = True
            sess = tf.Session(config=config)

            saver.restore(sess,os.path.join(self.MODEL_PATH,'model.ckpt'))
                

            ops = {'pointclouds_pl': pointclouds_pl,
                   'labels_pl': labels_pl,
                   'global_pl':global_pl,
                   'is_training_pl': is_training_pl,
                   'pred': pred,
            }
            

        
            def eval_one_epoch(sess,ops):
                is_training = False
                feed_dict = {
                    ops['pointclouds_pl']: data['pointclouds_pl'],
                    ops['is_training_pl']: is_training,
                    ops['global_pl']:data['global_pl'],
                }
                pred = sess.run([ops['pred']],feed_dict=feed_dict)
                pred = np.squeeze(pred)
                if self.BATCH_SIZE ==1:
                    pred = np.expand_dims(pred, axis=0)                    
                    
                return pred
            


            return eval_one_epoch(sess,ops)
################################################          
    

if __name__=='__main__':
  eval()
