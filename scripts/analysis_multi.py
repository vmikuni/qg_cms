from coffea import hist
import uproot
from coffea.util import awkward
from coffea.analysis_objects.JaggedCandidateArray import  JaggedCandidateArray
import coffea.processor as processor
from coffea.processor import run_parsl_job
from coffea.processor.parsl.parsl_executor import parsl_executor
import numpy as np
import utils
import matplotlib.pyplot as plt
import sys
import re
import h5py as h5
from optparse import OptionParser
import parsl
import os
from parsl.configs.local_threads import config
from parsl.providers import LocalProvider,SlurmProvider
from parsl.channels import LocalChannel,SSHChannel
from parsl.config import Config
from parsl.executors import HighThroughputExecutor
from parsl.launchers import SrunLauncher
from parsl.addresses import address_by_hostname



def Writeh5(output, name,folder):
    if not os.path.exists(folder):
        os.makedirs(folder)    
    with h5.File(os.path.join(folder,'QG_{0}.h5'.format(name)), "w") as fh5:
        dset = fh5.create_dataset("data", data=output['data'])
        dset = fh5.create_dataset("pid", data=output['pid'])
        dset = fh5.create_dataset("global", data=output['global'])

class Channel():
    def __init__(self, name):
        self.name = name
        self.sel=None
        self.jets= None
        self.pfs=None
        self.pfs_pid=None
        self.glob = None
        self.jet = None
        self.pid = None
        

    def apply_jet_sel(self,quark='up'):
        jet_matched_pfs = self.jets[self.pfs.idx]

        #print(gen_electrons.pdgId)
        #Cuts on jets

        if quark=='up':
            pfs_mask = (np.abs(jet_matched_pfs.partonFlavour)==2) & (jet_matched_pfs.hadronFlavour==0)
        elif quark == 'strange':
            pfs_mask = (np.abs(jet_matched_pfs.partonFlavour)==3) & (jet_matched_pfs.hadronFlavour==0)
        elif quark == 'down':
            pfs_mask = (np.abs(jet_matched_pfs.partonFlavour)==1) & (jet_matched_pfs.hadronFlavour==0)
        else:
            pfs_mask = (np.abs(jet_matched_pfs.partonFlavour)==21) & (jet_matched_pfs.hadronFlavour==0)
            
        self.pfs = self.pfs[pfs_mask]
        


    def datah5(self):         
        sys.path.append(os.path.join(os.environ['COFFEAHOME'],'scripts'))    
        import coffea_utils as cutils

        if self.pfs.shape[0] ==0: 
            return [],[]
        else:
            return cutils._datah5_PF(self.pfs,self.raw_jets,opt.points)
        
    

class SignalProcessor(processor.ProcessorABC):

    def __init__(self):
        dataset_axis = hist.Cat("dataset", "")        
        nparts_axis = hist.Bin("nparts", "N$_{particles}$ ", 100, 0, 100)        
        pt_axis = hist.Bin("pt", "p_T", 50, 20, 100)        

        axis_list = {
            'nparts':nparts_axis, 
            'pt':pt_axis, 
        }

        self.channels = ['up','down','strange','gluons']
        dict_accumulator = {}
        ML_accumulator = {
            'data': processor.list_accumulator(),
            'pid': processor.list_accumulator(),
            'global': processor.list_accumulator(),
        }
        dict_accumulator['ML'] = processor.dict_accumulator(ML_accumulator)

        for axis in axis_list:
            dict_accumulator["{}".format(axis)] = hist.Hist("Events", dataset_axis, axis_list[axis])
            
        

        for axis in axis_list:
            for channel in self.channels:
                dict_accumulator["{}_{}".format(axis,channel)] = hist.Hist("Events", dataset_axis, axis_list[axis])        
               
        dict_accumulator['cutflow']= processor.defaultdict_accumulator(int)

        self._accumulator = processor.dict_accumulator( dict_accumulator )
        

    @property
    def accumulator(self):
        return self._accumulator
    
        
    def process(self, df):
        sys.path.append(os.path.join(os.environ['COFFEAHOME'],'scripts')) 
        import coffea_utils as cutils

        output = self.accumulator.identity()
        dataset = df["dataset"]
        
        Jets = JaggedCandidateArray.candidatesfromcounts(
            df['nJet'],            
            pt=df['Jet_pt'].content,
            rawf=df['Jet_rawFactor'].content,
            eta=df['Jet_eta'].content,
            phi=df['Jet_phi'].content,
            mass=df['Jet_mass'].content,
            qgl=df['Jet_qgl'].content,
            btag=df['Jet_btagDeepFlavB'].content,
            partonFlavour=df['Jet_partonFlavour'].content,
            hadronFlavour=df['Jet_hadronFlavour'].content,
            cleanMask = df['Jet_cleanmask'].content,
            pu_id = df['Jet_puId'].content,
            nParts = df['Jet_nConstituents'].content,
            jet_id= df['Jet_jetId'].content,
            )
        

        PFCands = JaggedCandidateArray.candidatesfromcounts(
            df['nPFCands'],
            pt=df['PFCands_pt'].content,
            eta=df['PFCands_eta'].content,
            phi=df['PFCands_phi'].content,
            mass=df['PFCands_mass'].content,
            trkChi2=df['PFCands_trkChi2'].content,            
            charge=df['PFCands_charge'].content,                                    
            pdgId=df['PFCands_pdgId'].content,                                    
            )

        match_mask = (df["JetPFCands_pFCandsIdx"] < PFCands.counts) & (df["JetPFCands_jetIdx"] < Jets.counts) & (df["JetPFCands_pFCandsIdx"] > -1)
        PFCands = PFCands[df["JetPFCands_pFCandsIdx"][match_mask]].deepcopy()
        PFCands.add_attributes(idx=df["JetPFCands_jetIdx"][match_mask])
            

        output['cutflow']['all events'] += Jets.size
        output['cutflow']['all PFcands'] += PFCands.counts.sum()
        output['cutflow']['all Jets'] += Jets.counts.sum()

        
        # # # # # # # # # #
        # EVENT SELECTION #
        # # # # # # # # # #

        raw_Jets = Jets
        _, JetMask = cutils.ObjSelection(Jets,'jet',2017)        

        pfMask =JetMask[PFCands.idx]
        PFCands = PFCands[pfMask] #Keep only PF associated to clean jet
        

        output['cutflow']['filtered PFCands'] += PFCands.counts.sum()
        up = Channel("up")
        down = Channel("down")
        strange = Channel("strange")
        gluons = Channel("gluons")
        nevts=[]
        for ic, channel in enumerate([up,down,strange,gluons]):
            channel.pfs = PFCands
            channel.jets = raw_Jets
            channel.apply_jet_sel(channel.name)
            
            #Require at least 1 PF candidate

            channel.jets = channel.jets[channel.pfs.idx]
            pf_mask = channel.pfs.counts > 0
            channel.pfs = channel.pfs[pf_mask]
            if channel.pfs.size ==0: continue #No jet with that flavour found
            channel.jets = channel.jets[pf_mask]
            channel.raw_jets = raw_Jets[pf_mask]


            output['cutflow']['Jet filtered {}'.format(channel.name)] += channel.pfs.counts.sum()

            #Picking the first one for simplicity
            leading_jet_pt = channel.jets.pt[:,0]
            leading_jet_mask = channel.raw_jets.pt == leading_jet_pt            
            pf_leading_jet_mask =leading_jet_mask[channel.pfs.idx]            
            channel.pfs = channel.pfs[pf_leading_jet_mask]

            
            output['cutflow']['First jet {}'.format(channel.name)] += channel.pfs.size            
            channel.jet = channel.raw_jets[leading_jet_mask]
            output['pt_{}'.format(channel.name)].fill(dataset=dataset, pt=channel.jet.pt[:,0].flatten())
            
            channel.glob = np.concatenate((
                np.expand_dims(np.log(channel.jet.pt[:,0]),axis=-1),
                np.expand_dims(channel.jet.eta[:,0],axis=-1),
            ),axis=1)

            channel.pid = ic*(channel.jet.mass[:,0] > -1)
            nevts.append(channel.jet.size)

        #balance the number of training events
        

        nmin = min(nevts)
        
        if nmin>0:
            for channel in [up,down,strange,gluons]:
                output['ML']['data']+=channel.datah5()[:nmin]
                output['ML']['pid']+=channel.pid.tolist()[:nmin]
                output['ML']['global']+=channel.glob.tolist()[:nmin]
                                
        return output

    def postprocess(self, accumulator):
        return accumulator

def ZeroPad(values,zeros):
    for ival, value in enumerate(values):    
        zeros[ival][:len(value)] += np.array(value)
    return zeros


def GetSamplename(first_file):
    sname = ''
    for string in first_file.split('/'):        
        if '13TeV' in string or 'Run' in string:
            if 'Run' in string:
                sname = string.split('-')[0]
            else:
                sname = string
            break
    if sname == '': 
        print(first_file)
        print('ERROR: Cannot find the sample name')
        sys.exit()
    return utils.samples[sname]['name']




        

parser = OptionParser(usage="%prog [opt]  inputFiles")

parser.add_option("--samples",dest="samples", type="string", default='train', help="Specify which default samples to run [data/mc/all]. Default: data")
#parser.add_option("--plot",dest="plot", action="store_true", default=False, help="Make ratio plot at the end") #to be done
parser.add_option("-q", "--queue",  dest="queue", type="string", default="quick", help="Which queue to send the jobs. Default: %default")
parser.add_option("--mem",  dest="mem", type="long", default=1000, help="Memory required in mb")
parser.add_option("--points", type="int", default=100, help="max number of particles to be used. Default %default")
parser.add_option("--cpu",  dest="cpu", type="long", default=4, help="Number of cpus to use. Default %default")
parser.add_option("--blocks",  dest="blocks", type="long", default=100, help="number of blocks. Default %default")
parser.add_option("--walltime",  dest="walltime", type="string", default="0:59:50", help="Max time for job run. Default %default")
parser.add_option("--chunk",  dest="chunk", type="long",  default=50000, help="Chunk size. Default %default")
parser.add_option("--maxchunk",  dest="maxchunk", type="long",  default=2e6, help="Maximum number of chunks. Default %default")
parser.add_option("--version",  dest="version", type="string",  default="", help="nametag to append to output file.")
parser.add_option("--no_parsl",  dest="no_parsl", action="store_true",  default=False, help="Run without parsl. Default: False")

parser.add_option("--h5folder", type="string", default="h5", help="Folder to store the h5 files. Default: %default")

(opt, args) = parser.parse_args()
samples = opt.samples


if len(args) < 1:
    if 'COFFEADATA' not in os.environ:
        print("ERROR: Enviroment variables not set. Run setup.sh first!")
        sys.exit()
      
    if samples == 'train': 
        file_name = os.path.join(os.environ['COFFEADATA'],'QG_train_HT.txt')    
        fout_name = 'train_{}.root'.format(opt.version)
    elif samples == 'test': 
        file_name = os.path.join(os.environ['COFFEADATA'],'QG_test_HT.txt')    
        fout_name = 'test_{}.root'.format(opt.version)
    elif samples == 'eval': 
        file_name = os.path.join(os.environ['COFFEADATA'],'QG_eval_HT.txt')    
        fout_name = 'eval_{}.root'.format(opt.version)
    else:
        print("ERROR: You must specify what kind of dataset you want to run [--samples]")
    
    print('Loading sets from file')    
    files = []
    with open(os.path.join(file_name),'r') as fread:
        files = fread.readlines()
        files = [x.strip() for x in files] 
        idx = np.arange(len(files))
        np.random.shuffle(idx)
        files = np.array(files)[idx]
                    
else:
    files = args
    fout_name = 'out.root'

fileset={}
for f in files:
    name = GetSamplename(f)
    if name in fileset:
        fileset[name].append(f)
    else:
        fileset[name] = [f]

ismc = False
for sample in fileset:
    print('Files: {0}, sample name: {1}'.format(fileset[sample],sample))
    if 'data' not in sample:
        ismc = True

#if ismc: evaluator = GetEvaluator()



sched_options = '''
##SBATCH --account=gpu_gres 
##SBATCH --partition=gpu    
#SBATCH --cpus-per-task=%d
#SBATCH --mem=%d
##SBATCH --exclude=t3wn56
''' % (opt.cpu,opt.mem) 

x509_proxy = '.x509up_u%s'%(os.getuid())
wrk_init = '''
export XRD_RUNFORKHANDLER=1
export X509_USER_PROXY=/t3home/%s/%s
'''%(os.environ['USER'],x509_proxy)



if opt.no_parsl:
    output = processor.run_uproot_job(fileset,
                                      treename='Events',
                                      processor_instance=SignalProcessor(),
                                      executor=processor.futures_executor,
                                      executor_args={'workers':opt.cpu},
                                      maxchunks =opt.maxchunk,
                                      chunksize = opt.chunk
    )


else:
    print("id: ",os.getuid())
    print(wrk_init)
    #sched_opts = ''
    #wrk_init = ''


    slurm_htex = Config(
        executors=[
            HighThroughputExecutor(
                label="coffea_parsl_slurm",
                #worker_debug=True,
                address=address_by_hostname(),
                prefetch_capacity=0,  
                heartbeat_threshold=60,
                cores_per_worker=1,
                #cores_per_worker=opt.cpu,
                max_workers=opt.cpu,
                provider=SlurmProvider(
                    channel=LocalChannel(),
                    launcher=SrunLauncher(),
                    init_blocks=opt.blocks,
                    min_blocks = opt.blocks-20,                     
                    max_blocks=opt.blocks+20,
                    exclusive  = False,
                    parallelism=1,
                    nodes_per_block=1,
                    #cores_per_node = opt.cpu,
                    partition=opt.queue,
                    scheduler_options=sched_options,   # Enter scheduler_opt if needed
                    worker_init=wrk_init,         # Enter worker_init if needed
                    walltime=opt.walltime
                ),
            )
        ],
        initialize_logging=False,
        app_cache = True,
        retries=2,
        strategy=None,
    )

    dfk = parsl.load(slurm_htex)


    output = processor.run_uproot_job(fileset,
                                      treename='Events',
                                      processor_instance=SignalProcessor(),
                                      executor=processor.parsl_executor,
                                      executor_args={'config':None, 
                                                     'flatten': False,'tailtimeout':60,'xrootdtimeout':60},
                                      chunksize = opt.chunk,
                                      maxchunks =opt.maxchunk,
    )

    #np.save('test.npy', output)
for flow in output['cutflow']:
    print(flow, output['cutflow'][flow])

Writeh5(output['ML'],opt.version,os.path.join(os.environ['COFFEAHOME'],opt.h5folder))
    

if not os.path.exists(os.path.join(os.environ['COFFEAHOME'],"pods")):
    os.makedirs(os.path.join(os.environ['COFFEAHOME'],"pods"))

fout = uproot.recreate(os.path.join(os.environ['COFFEAHOME'],"pods",fout_name))

for var in output:
    if var == 'cutflow' or var == 'ML':continue
    for dataset in fileset:
        if len(output[var][dataset].sum("dataset").values()) < 1: continue #Don't try to save an empty histogram
        fout["{0}_{1}".format(dataset,var)] = hist.export1d(output[var][dataset].sum("dataset"))
fout.close()

if not opt.no_parsl:
    parsl.dfk().cleanup()
    parsl.clear()
