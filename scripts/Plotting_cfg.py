import ROOT as rt
rt.PyConfig.IgnoreCommandLineOptions = True
histLineColor = rt.kBlack
markerSize  = 1.0
histFillColor =['#e41a1c','#377eb8','#4daf4a','#984ea3','#ff7f00','#a65628','#f781bf','#999999','#ffff33','#e41a1c','#377eb8','#4daf4a','#984ea3',]

import array


#CMS Style
H_ref = 600;
W_ref = 540;
x1_l = 0.9
y1_l = 0.90
dx_l = 0.25
dy_l = 0.28
x0_l = x1_l-dx_l
y0_l = y1_l-dy_l
ar_l = dy_l/dx_l
W = W_ref
H  = H_ref
# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref
L = 0.12*W_ref
R = 0.04*W_ref

gap_ = 1.0/5
bwx_ = 0.14/2
bwy_ = gap_/1.5

x_l = [1.2*bwx_]

y_l = [1-gap_]
ex_l = [0]
ey_l = [0.04/ar_l]


x_l = array.array("f",x_l)
ex_l = array.array("f",ex_l)
y_l = array.array("f",y_l)
ey_l = array.array("f",ey_l)
xx_ = x_l[0]



#########################################################
#Plot specific quantities
#Ratio specific
RTSX = 0.156 #ratio title size
RTOX = 1.1 #ratio title offset
RLSX = 0.161 #ratio label size
RTSY = 0.166 #ratio title size
RTOY = 0.3 #ratio title offset
RLSY = 0.141 #ratio label size
RMIN = 0.7
RMAX = 1.3


#Second pad quantities
P2RM = 0.04 #pad right margin
P2LM = 0.12 #pad left margin
P2TM = 0.05  #pad top margin
P2BM = 0.42  #pad bottom margin
#Legend specific
xgap = -0.45
n_ = 5/2 #Number of items on legend
#gap_ = 1./(n_+1)
gap_ = 1./(n_+1)
#ygap = -0.45
xshiftm = -0.2
xshiftp = 0
yshiftm =+0.02
yshiftp = 0.00
legendsize = 0.13
#Frame specific
FLS = 0.05
FTS = 0.055
FTO = 0.98
FRML = 1.6 #linear maximum frame range multiplyier
FRMLOG = 4e3 #log maximum frame range multiplyier
ranges = 1000
bins = 50
