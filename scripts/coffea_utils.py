import sys
import numpy as np


def ScaleH(output,fileset):
    ''' Scale histograms to lumi'''
    import utils
    lumi = utils.lumi
    for var in output:
        if 'cutflow' in var or 'TTree' in var: continue        
        for dist in fileset:
            #print(dist)
            if 'data' in dist:                 
                continue                            
            else:
                sname = ''
            for sample in utils.samples:
                if utils.samples[sample]['name'] == dist: #reverse search for sample 
                    sname = sample                    
                    break
            if sname == '': 
                sys.exit('ERROR: Could not find the sample!')
                
            scale = lumi*utils.samples[sname]['xsec']/utils.samples[sname]['ngen']
            output[var].scale({dist:scale},axis='dataset')
    return output



def ObjSelection(obj,name,year=2016,selection=None):
    '''Apply standard selection to ojects.
    Inputs: 
    obj: jagged_array to apply the selection
    name: [jet,electron,muon,PF,tight_muon]
    selection: bool mask for selection different than the standard

    Returns: obj array with selection applied '''

    if year != 2017 and year != 2016: #FixMe add other years
        sys.exit("ERROR: Only 2016 and 2017 supported")

    obj_list = ['jet','electron','muon','PF','tight_muon','medium_muon']
    
    if name not in obj_list and selection == None:
        sys.exit("ERROR: For non standard objects, provide a selection mask to be applied")

    if selection != None:
        return obj[selection]
    else:
        if year == 2016:
            if name == 'jet':
                selection = (
                    (np.abs(obj.eta) < 2.4) & (obj.pt > 30)  & 
                    (
                        (obj.pt >= 50) 
                        | ((obj.pt < 50) & (obj.pu_id>=6) )
                    ) &
                    ((obj.jet_id==7)) & #tight
                    #(jets.b_tag >= 0) &
                    (obj.mass > -1) 
                )
            elif name == 'PF':
                selection = (
                    (obj.pt > 0.2)  & (np.abs(obj.eta)<2.4)  
                    & (obj.puppiWeight > 0.1)
                    & (obj.mass > -1) 
                )
                
            elif name == 'muon':
                selection = (
                    (np.abs(obj.eta) < 2.4) & (obj.pt > 10)  &
                    (obj['dxy'] < 0.2) &
                    (obj['dz'] < 0.5) &
                    (obj['iso'] < 0.15) &
                    (obj['isLoose']) &
                    (obj.mass > -1)  
                )
                
            elif name == 'tight_muon':
                selection = (
                    (np.abs(obj.eta) < 2.4) & (obj.pt > 20)  &
                    (obj['iso'] < 0.15) &
                    (obj['isTight']) &
                    (obj.mass > -1) 
                )
                
            elif name == 'electron':
                selection = (
                    (np.abs(obj.eta) < 2.4) & (obj.pt > 20)  &
                    (
                        ((np.abs(obj.eta) < 1.4442) & (np.abs(obj['dxy']) < 0.05) & (np.abs(obj['dz']) < 0.1) ) 
                        | ((np.abs(obj.eta) > 1.5660) & (np.abs(obj['dxy']) < 0.1) & (np.abs(obj['dz']) < 0.2) )
                    ) & 
                    (obj['cutbased']>=2) &
                    (obj.mass >-1)        
                )
                
        if year == 2017:
                
            if name == 'jet':
                selection = (
                    (np.abs(obj.eta) < 4.7) & 
                    (obj.pt > 20)  & 
                    ((obj.jet_id==6)) & #tight
                    #(obj.cleanMask==1) & 
                    (
                        (obj.pt >= 50) 
                        | ((obj.pt < 50) & (obj.pu_id>=0) )
                    ) &

                    #(jets.b_tag >= 0) &
                    (obj.mass > -1) 
                )
                
            elif name == 'PF':
                selection = (
                    (obj.pt > 0.0)
                    #& (obj.puppiWeight > 0.1)
                    & (obj.mass > -1) 
                )
                
            elif name == 'muon':
                selection = (
                    (np.abs(obj.eta) < 2.4) & (obj.pt > 10)  &
                    (obj['iso'] < 0.15) &
                    (obj['isLoose']) &
                    (obj.mass > -1)  
                )
                
            elif name == 'medium_muon':
                selection = (
                    (np.abs(obj.eta) < 2.5) & 
                    (obj.pt > 20)  &
                    (obj['iso'] < 0.15) &
                    (obj['isMedium']) &
                    (obj.mass > -1) 
                )

            elif name == 'tight_muon':
                selection = (
                    (np.abs(obj.eta) < 2.4) & 
                    (obj.pt > 20)  &
                    (obj['iso'] < 0.15) &
                    (obj['isTight']) &
                    (obj.mass > -1) 
                )
                
            elif name == 'electron':
                selection = (
                    (np.abs(obj.eta) < 2.4) & (obj.pt > 10)  &

                    (
                        ((np.abs(obj.eta) < 1.4442) & (np.abs(obj['dxy']) < 0.05) & (np.abs(obj['dz']) < 0.1) ) 
                        | ((np.abs(obj.eta) > 1.5660) & (np.abs(obj['dxy']) < 0.1) & (np.abs(obj['dz']) < 0.2) )
                    ) & 
                    (obj['cutbased']>=1) &
                    (obj.mass >-1)        
                )
        return obj[selection],selection
    



def _datah5_PF(pfs,jets,points=100):
    data = np.zeros((pfs.size,points,13))
    jet_pf = jets[pfs.idx]
    #print(jet_pf.partonFlavour)
    dr_pf = pfs['p4'].delta_r(jet_pf['p4'])
    dphi_pf = pfs['p4'].delta_phi(jet_pf['p4'])
    
    data[:,:,0]+=(jet_pf.eta - pfs.eta).pad(points, clip=True).fillna(0).regular()
    data[:,:,1]+=dphi_pf.pad(points, clip=True).fillna(0).regular()
    data[:,:,2]+=np.log(pfs.pt).pad(points, clip=True).fillna(0).regular()
    data[:,:,3]+=np.log(pfs.p4.energy).pad(points, clip=True).fillna(0).regular()
    data[:,:,4]+=np.log(pfs.pt/jet_pf.pt).pad(points, clip=True).fillna(0).regular()
    data[:,:,5]+=np.log(pfs.p4.energy/jet_pf.p4.energy).pad(points, clip=True).fillna(0).regular()
    data[:,:,6]+=dr_pf.pad(points, clip=True).fillna(0).regular()
    data[:,:,7]+=pfs.charge.pad(points, clip=True).fillna(0).regular()
    data[:,:,8]+=(np.abs(pfs.pdgId)==11).pad(points, clip=True).fillna(0).regular()
    data[:,:,9]+=(np.abs(pfs.pdgId)==13).pad(points, clip=True).fillna(0).regular()
    data[:,:,10]+=(np.abs(pfs.pdgId)==211).pad(points, clip=True).fillna(0).regular()
    data[:,:,11]+=(np.abs(pfs.pdgId)==130).pad(points, clip=True).fillna(0).regular()
    data[:,:,12]+=(np.abs(pfs.pdgId)==22).pad(points, clip=True).fillna(0).regular()


    return data.tolist()

