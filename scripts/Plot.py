import os,sys
import ROOT as rt
import  tdrstyle
import array
import utils
import Plotting_cfg as cfg
import copy 

import numpy as np
from math import *
import numpy as np
from collections import OrderedDict
from optparse import OptionParser
rt.PyConfig.IgnoreCommandLineOptions = True

rt.gROOT.SetBatch(True)
rt.gStyle.SetOptStat(0)
tdrstyle.setTDRStyle()
rt.TH1.SetDefaultSumw2()
rt.TH2.SetDefaultSumw2()

parser = OptionParser(usage="%prog [options]")
parser.add_option("-p","--plots",dest="plots", type="string", default="../plots", help="Path to store plots. [default: %default]")
parser.add_option("--name", type="string", default="test", help="basename of the resulting files. [default: %default]")
parser.add_option("-d","--dir", type="string", default="../pods", help="Base path for the folder with input root files. [default: %default]")
parser.add_option("-f","--file", type="string", default="qg_histograms.root", help="Name of the root file to load. [default: %default]")
parser.add_option("--plotData",dest="plotData",action="store_true", default=False, help="Plot the data with the simulation. [default: %default]")
parser.add_option("--logY",dest="logY",action="store_true", default=False, help="Plot in log scale. [default: %default]")


(options, args) = parser.parse_args()



plotData = options.plotData
fname =os.path.join(options.dir,options.name,options.file)
version = options.name
is_log = options.logY




f = rt.TFile(fname,"READ")

processlist = utils.processlist

ordered_procs = [
    'data',
    #'Z+Quarks',
    'Z+Others',
    'Z+Strange',
    'Z+Up',
    'Z+Down',
    'Z+Gluon',
] #Only these processes are shown


plots = {
    'njets':'N_{jets}',
    'QGL':'QGL',
    'DeepFlavQG':'DeepFlavQG',
    'Jet_pt':'p_{T} [GeV]',
    'Dimuon_mass': 'm(#mu#mu) [GeV]',
    'Dimuon_pt': 'p_{T}(#mu#mu) [GeV]',
    'Asymmetry_frac':'Jet asymmetry',
    'Delta_Phi':'#Delta#phi(Z,jet^1)',
    'ABCNet_up':'ABCNet(up)',
    'ABCNet_down':'ABCNet(down)',
    'ABCNet_strange':'ABCNet(strange)',
    'ABCNet_gluon':'ABCNet(gluon)',
}


hstack =  {}
hist_group = {}



for ip, plot in enumerate(plots):
    hstack[plot] = rt.THStack(plot,plot)
    hist_group[plot] = {}
    total_mc = total_data = 0
    for group in ordered_procs: #Loop through categories
        for process in processlist[group][0]: # Loop through each process belonging to each category
            if not f.GetListOfKeys().Contains("{}_{}".format(process,plot)) : 
                print("Process {} not found".format(process))
                continue
                #print("{}_{}_{}".format(process,plot,channel))
            if group in hist_group[plot]:                    
                    hist_group[plot][group].Add(copy.deepcopy(f.Get("{}_{}".format(process,plot))))
                    utils.AddOverflow(hist_group[plot][group])
            else:
                hist_group[plot][group] = copy.deepcopy(f.Get("{}_{}".format(process,plot)))
                utils.AddOverflow(hist_group[plot][group])
                
        if 'data' in group:
            hist_group[plot][group].SetMarkerColor(1)
            hist_group[plot][group].SetMarkerStyle(20)
            hist_group[plot][group].SetMarkerSize(1.0)
            total_data+=hist_group[plot][group].Integral()
                
        else:            
            hist_group[plot][group].SetFillColor(rt.TColor.GetColor(processlist[group][1]))
            hist_group[plot][group].SetLineColor(rt.kBlack)                
            total_mc+=hist_group[plot][group].Integral()
            
    for histogram in hist_group[plot]:                    
        if 'data'in histogram: continue
        hist_group[plot][histogram].Scale(total_data/total_mc) #Normalize MC to the data
        hstack[plot].Add(hist_group[plot][histogram])



    canvas = utils.StackPlot(hstack[plot],hist_group[plot],plots[plot],plotData,is_log)
    latex = rt.TPaveText(0.15, 0.7, 0.35, 0.8,"brNDC")
    #latex.SetTextFont(42)
    latex.SetTextAngle(0)
    latex.SetTextColor(rt.kBlack)
    latex.SetTextSize(0.03)
    latex.SetTextAlign(12)
    latex.SetFillStyle(0)
    latex.SetBorderSize(0)
    latex.SetFillColor(0)
    latex.AddText("{} GeV < pT < {} GeV".format(utils.pt_thresh_low,utils.pt_thresh_high))
    latex.AddText("|#eta| < {} ".format(utils.eta_thresh))
    latex.Draw("same")
    

    if not os.path.exists(os.path.join(options.plots,version)):
        os.makedirs(os.path.join(options.plots,version))
    else:
        print ("WARNING: directory already exists. Will overwrite existing files...")
    canvas.SaveAs("{}/{}/{}.pdf".format(options.plots,version,plot))

        
