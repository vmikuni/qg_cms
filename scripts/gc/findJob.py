import sys, os

path = './work.qg/'
txts = []
for root, dirs, files in os.walk(path):
    for f in files:
        if ".txt" in f:
            txts.append(os.path.join(root,f))

problem_jobs = []
status = {}
for txt in txts:
    with open(txt,'r') as f:
        for line in f:
            if "status" in line:
                stat = line.split("=")[1]
                if "UNKNOWN" in stat or "TIME" in stat:
                    jobid = txt.split('/')[-1].replace(".txt","")
                    jobid=jobid.split("_")[-1]
                    problem_jobs.append(int(jobid))        
                if stat in status:
                    status[stat]+=1
                else:
                    status[stat] = 0

for stat in status:
    print ("{0}:{1}".format(stat,status[stat]/2))

problems = list(dict.fromkeys(problem_jobs))
jobs = ''
for problem in problems:
    jobs+='{0},'.format(problem)
problems = str(problems).strip('[]  ')
print ("/t3home/vmikuni/grid-control/go.py run_qg.conf --reset {0}".format(jobs))

