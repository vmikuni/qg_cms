from coffea import hist
import uproot
from coffea.util import awkward
from coffea.analysis_objects import JaggedCandidateArray
import coffea.processor as processor
from coffea.processor import run_parsl_job
from coffea.processor.parsl.parsl_executor import parsl_executor
from coffea.lookup_tools import extractor
from coffea.lumi_tools import LumiMask
import numpy as np
import matplotlib.pyplot as plt
import utils
import sys
import re
import h5py as h5
from optparse import OptionParser
import parsl
import os
from parsl.configs.local_threads import config
from parsl.providers import LocalProvider,SlurmProvider
from parsl.channels import LocalChannel,SSHChannel
from parsl.config import Config
from parsl.executors import HighThroughputExecutor
from parsl.launchers import SrunLauncher
from parsl.addresses import address_by_hostname
import json, yaml
import copy
import coffea_utils as cutils
from Evaluate_ABC import ABCModel



def datah5(pfs,raw_jets):         
    if pfs.shape[0] ==0: 
        return [],[]
    else:            
        return cutils._datah5_PF(pfs,raw_jets)
        

class SignalProcessor(processor.ProcessorABC):
    def __init__(self):        
        #sys.path.append(os.path.join(os.environ['COFFEAHOME'],'scripts'))    

        #Define the histograms to save
        dataset_axis = hist.Cat("dataset", "")        
        ABC_axis = hist.Bin("ABC", "ABCNet output", 50, 0, 1)        
        QG_axis = hist.Bin("QG", "QGL output", 50, 0, 1)        


        #parton_axis = hist.Bin("parton", "Parton flavour", 22, 0,22)        
        

        axis_list = {
            'ABC':ABC_axis, 
            'QGL':QG_axis, 
        }

        
        self.channels = ['Zj'] 
        self.run_dict = {}
        dict_accumulator = {}

        list_accumulator = { #Output TTrees will contain the following variables
            'njets': processor.list_accumulator(),
            'weight': processor.list_accumulator(),
            'QGL': processor.list_accumulator(),
            'DeepFlavQG': processor.list_accumulator(),
            'ABCNet_up': processor.list_accumulator(),
            'ABCNet_down': processor.list_accumulator(),
            'ABCNet_strange': processor.list_accumulator(),
            'ABCNet_gluon': processor.list_accumulator(),
            
            'Dimuon_mass':processor.list_accumulator(),
            'Dimuon_pt':processor.list_accumulator(),
            #'Asymmetry_frac':processor.list_accumulator(),
            #'Delta_Phi':processor.list_accumulator(),
            'Jet_partonFlavour':processor.list_accumulator(),
            'Jet_hadronFlavour':processor.list_accumulator(),
            'Jet_pt':processor.list_accumulator(),
            'Jet_eta':processor.list_accumulator(),
            'Jet_phi':processor.list_accumulator(),
            'Jet_mass':processor.list_accumulator(),
        }
        
        dict_accumulator['TTree'] = processor.dict_accumulator(list_accumulator)

        for axis in axis_list:
            for channel in self.channels:
                dict_accumulator["{}_{}".format(axis,channel)] = hist.Hist("Events", dataset_axis, axis_list[axis])        
        
        dict_accumulator['cutflow']= processor.defaultdict_accumulator(int)
        #print(dict_accumulator)

        self._accumulator = processor.dict_accumulator( dict_accumulator )
    
    
    @property
    def accumulator(self):
        
        return self._accumulator
    
        
    def process(self, df):        
        # sys.path.append(os.path.join(os.environ['COFFEAHOME'],'scripts'))    
        # import coffea_utils as cutils
        output = self.accumulator.identity()


        dataset = df["dataset"]
        
        
        #Properties per object
        jets = JaggedCandidateArray.candidatesfromcounts(
            df['nJet'],
            #pt=df['Jet_pt'].content,            
            pt=df['Jet_pt_nom'].content,            
            eta=df['Jet_eta'].content,
            phi=df['Jet_phi'].content,
            mass=df['Jet_mass_nom'].content,
            #mass=df['Jet_mass'].content,
            jet_id=df['Jet_jetId'].content, #this one is bitwise for some reason
            partonFlavour= df['Jet_partonFlavour'].content if 'Jet_partonFlavour' in df else np.ones(df['Jet_jetId'].content.shape), #dummy flag for data
            hadronFlavour= df['Jet_hadronFlavour'].content if 'Jet_hadronFlavour' in df else np.ones(df['Jet_jetId'].content.shape), #dummy flag for data
            DeepFlavQG  = df['Jet_btagDeepFlavQG'].content,
            cleanMask = df['Jet_cleanmask'].content,
            pu_id = df['Jet_puId'].content,

            qgl=df['Jet_qgl'].content,

            )

        muons = JaggedCandidateArray.candidatesfromcounts(
            df['nMuon'],
            pt=df['Muon_pt'].content,
            eta=df['Muon_eta'].content,
            phi=df['Muon_phi'].content,
            mass=df['Muon_mass'].content,
            charge=df['Muon_charge'].content,
            iso=df['Muon_pfRelIso04_all'].content,
            dxy=df['Muon_dxy'].content,
            dz=df['Muon_dz'].content,            
            isTight=df['Muon_tightId'].content,
            isMedium=df['Muon_mediumId'].content,
            isLoose=df['Muon_looseId'].content,
            )

        electrons = JaggedCandidateArray.candidatesfromcounts(
            df['nElectron'],
            pt=df['Electron_pt'].content,
            eta=df['Electron_eta'].content,
            phi=df['Electron_phi'].content,
            mass=df['Electron_mass'].content,
            charge=df['Electron_charge'].content,
            dxy=df['Electron_dxy'].content,
            iso=df['Electron_pfRelIso03_all'].content,
            dz=df['Electron_dz'].content,
            cutbased=df['Electron_cutBased'].content,
            )

        PFCands = JaggedCandidateArray.candidatesfromcounts(
            df['nPFCands'],
            pt=df['PFCands_pt'].content,
            eta=df['PFCands_eta'].content,
            phi=df['PFCands_phi'].content,
            mass=df['PFCands_mass'].content,
            trkChi2=df['PFCands_trkChi2'].content,
            charge=df['PFCands_charge'].content,
            pdgId=df['PFCands_pdgId'].content,
            d0 = df['PFCands_d0'].content,
            dz = df['PFCands_dz'].content,
            puppiWeight = df['PFCands_puppiWeight'].content,
            #idx = df['PFCands_jetIdx'].content,
        )
        
        match_mask = (df["JetPFCands_pFCandsIdx"] < PFCands.counts) & (df["JetPFCands_jetIdx"] < jets.counts) & (df["JetPFCands_pFCandsIdx"] > -1)
        PFCands = PFCands[df["JetPFCands_pFCandsIdx"][match_mask]].deepcopy()
        PFCands.add_attributes(idx=df["JetPFCands_jetIdx"][match_mask])

        # if 'data' not in dataset:            
        #     lumi = np.ones(electrons.size,dtype=np.bool_)
        # else:
        #     if 'COFFEADATA' not in os.environ:
        #         print("ERROR: Enviroment variables not set. Run setup.sh first!")
        #         sys.exit()
        #     json_path = os.path.join(os.environ['COFFEADATA'],'json')

        #     run = df["run"]
        #     lumiblock = df["luminosityBlock"]
        #     lumi_mask = LumiMask('{}/Cert_294927-306462_13TeV_UL2017_Collisions17_GoldenJSON.txt'.format(json_path))
        #     lumi = lumi_mask(run,lumiblock)
                    

            
        #input()
        # This keeps track of how many events there are, as well as how many of each object exist in this events.
        output['cutflow']['all events'] += muons.size
        output['cutflow']['all jets'] += jets.counts.sum()
        output['cutflow']['all muons'] += muons.counts.sum()
        output['cutflow']['all electrons'] += electrons.counts.sum()
        
        # # # # # # # # # # #
        # TRIGGER SELECTION #
        # # # # # # # # # # #
        
        #FixMe: year dependent flags

            
        trigger_mask = (df['HLT_IsoMu24']==1)  #IsoTrk missing


        # # # # # # # # # # #
        # OBJECT SELECTIONS #
        # # # # # # # # # # #
        
        # ONLY consider jets with pt > 20. Note that we don't want to down-select our jets (we wish to preserve jaggedness). The JaggedArray does this if more than one argument is provided for the mask. Since jets.mass > -1 is arbitrarily true, we add it to avoid this down-selection.
        muons,_ = cutils.ObjSelection(muons,'medium_muon',2017)        
        electrons,_ = cutils.ObjSelection(electrons,'electron',2017)        
        raw_jets = jets
        _, JetMask = cutils.ObjSelection(jets,'jet',2017)        
                

        # Now we want to make sure no jets are within 0.4 delta-R of any muon.
        # We cross jets with both fakeable muons, keeping it nested so it groups together by each jet and we can check if each jet is far enough away from every lepton.

        cross_mu = raw_jets['p4'].cross(muons['p4'], nested=True)
        # delta_r is a built-in function for TLorentzVectors, so we'll make use of it.
        check_jmu = (cross_mu.i0.delta_r(cross_mu.i1) > 0.4).all()

        cross_e = raw_jets['p4'].cross(electrons['p4'], nested=True)
        # delta_r is a built-in function for TLorentzVectors, so we'll make use of it.
        check_je = (cross_e.i0.delta_r(cross_e.i1) > 0.4).all()



        JetMask = (JetMask) & (check_je) & (check_jmu)
        jets = raw_jets[JetMask]
        
        pfMask =JetMask[PFCands.idx]
        PFCands = PFCands[pfMask] #Keep only PF associated to clean jet
        PFCands,_ = cutils.ObjSelection(PFCands,'PF',2017)


        
    
        # This tells us how many objects pass each of our cuts.
        output['cutflow']['electrons'] += electrons.counts.sum()
        output['cutflow']['muons'] += muons.counts.sum()
        output['cutflow']['jets'] += jets.counts.sum()
        
        # # # # # # # # # #
        # EVENT SELECTION #
        # # # # # # # # ## 
        

        jetMask = (jets.counts>=1) 
        eventMask = (jetMask)  & (muons.counts >= 2) & (trigger_mask)  #Event selection


        muons = muons[eventMask]
        jets = jets[eventMask]
        PFCands = PFCands[eventMask]
        raw_jets = raw_jets[eventMask]
        
        
        leading_jet_pt = jets.pt[:,0]
        leading_jet_mask = raw_jets.pt == leading_jet_pt
        pf_leading_jet_mask =leading_jet_mask[PFCands.idx] 
        #Keeping only the PF candidates associated to the leading jet
        PFCands = PFCands[pf_leading_jet_mask]

        #Apply a selection to keep Z(mumu) events only
        #From https://indico.cern.ch/event/940358/contributions/3951243/attachments/2079363/3492369/QGL_meeting_24_7_2020_kallonen.pdf
        #March 2021: Update selection to this one:https://cms.cern.ch/iCMS/jsp/db_notes/noteInfo.jsp?cmsnoteid=CMS%20AN-2019/098

        z_cand = muons[:,:2].distincts()
        
        z_charge = muons[:,0].charge*muons[:,1].charge
            
        cross = jets['p4'].cross(z_cand['p4'])                        
        z_jet_deltaphi = cross.i0.delta_phi(cross.i1)




        mass_mask = (z_cand.mass >70) & (z_cand.mass <110)
        charge_mask = z_charge<0

        deltaphi_mask = np.abs(z_jet_deltaphi) > 2.0
        deltaphi_mask = deltaphi_mask[:,0] #Only care about leading jet
        asymmetry_mask= np.abs(jets.pt[:,0]-z_cand.pt)/(jets.pt[:,0]+z_cand.pt) < 0.3


        
        dilepton_mask = (mass_mask.flatten()) & (charge_mask.flatten()) & (deltaphi_mask) & (asymmetry_mask.flatten())
        
        muons = muons[dilepton_mask]
        jets = jets[dilepton_mask]
        PFCands = PFCands[dilepton_mask]
        raw_jets = raw_jets[dilepton_mask]
        z_cand = z_cand[dilepton_mask]
        z_jet_deltaphi = z_jet_deltaphi[dilepton_mask]

            
        
        
        if muons.shape[0] >0:
            abc_model = ABCModel('multi_train',jets.size)
            glob = np.concatenate((
                np.expand_dims(np.log(jets.pt[:,0]),axis=-1),
                np.expand_dims(jets.eta[:,0],axis=-1),
            ),axis=1)
            abc_inputs=datah5(PFCands,raw_jets)
        
            feed_eval = {
                'pointclouds_pl': abc_inputs,
                'global_pl':glob,
            }
            pred_flavour = abc_model.eval(feed_eval)

        
            if 'data' not in dataset:
                genWeights = (df['genWeight'])[(eventMask)]
                puWeights = (df['puWeight'])[(eventMask)]
                weights = genWeights*puWeights
                weights = weights[(dilepton_mask)]
                #jet_flavour = jets.partonFlavour[:,0]

            else:
                weights=np.ones(jets.size)
                #jet_flavour=np.ones(jets.size)

            output['cutflow']['Dilepton selection'] += jets.size
                        
            #Saves information to TTrees
            output['TTree']['njets']+=jets.counts.tolist()
            output['TTree']['weight']+=weights.tolist()
            output['TTree']['Jet_partonFlavour']+=jets.partonFlavour[:,0].tolist()
            output['TTree']['Jet_hadronFlavour']+=jets.hadronFlavour[:,0].tolist()
            output['TTree']['QGL']+=jets.qgl[:,0].tolist()
            output['TTree']['DeepFlavQG']+=jets.DeepFlavQG[:,0].tolist()
            output['TTree']['Jet_pt']+=jets.pt[:,0].tolist()
            output['TTree']['Jet_eta']+=jets.eta[:,0].tolist()
            output['TTree']['Jet_phi']+=jets.phi[:,0].tolist()
            output['TTree']['Jet_mass']+=jets.mass[:,0].tolist()
            
            output['TTree']['Dimuon_mass']+=z_cand.mass.tolist()
            output['TTree']['Dimuon_pt']+=z_cand.pt.tolist()
            # output['TTree']['Asymmetry_frac']+= (np.abs(jets.pt[:,0]-z_cand.pt)/(jets.pt[:,0]+z_cand.pt)).tolist()
            # output['TTree']['Delta_Phi']+= (z_jet_deltaphi[:,0]).tolist()

            output['TTree']['ABCNet_up']+=pred_flavour[:,0].tolist()
            output['TTree']['ABCNet_down']+=pred_flavour[:,1].tolist()
            output['TTree']['ABCNet_strange']+=pred_flavour[:,2].tolist()
            output['TTree']['ABCNet_gluon']+=pred_flavour[:,3].tolist()

    
                        
        return output

    def postprocess(self, accumulator):
        return accumulator


def GetSamplename(first_file,base_folder):

    folder_idx = list(first_file.split('/')).index(base_folder)
    sname = list(first_file.split('/'))[folder_idx+1]
    
    if sname == '': 
        print(first_file)
        print('ERROR: Cannot find the sample name')
        sys.exit()
    return utils.samples[sname]['name']

        
parser = OptionParser(usage="%prog [opt]  inputFiles")

parser.add_option("--samples",dest="samples", type="string", default='data', help="Specify which default samples to run [data/mc/all]. Default: data")
#parser.add_option("--plot",dest="plot", action="store_true", default=False, help="Make ratio plot at the end") #to be done
parser.add_option("-q", "--queue",  dest="queue", type="string", default="quick", help="Which queue to send the jobs. Default: %default")
parser.add_option("--base_folder", type="string", default="UL2017_QG", help="Folder which the data is stored. Default: %default")
parser.add_option("-p", "--nproc",  dest="nproc", type="long", default=1, help="Number of processes to use. Default: %default")
parser.add_option("--mem",  dest="mem", type="long", default=1100, help="Memory required in mb")
parser.add_option("--cpu",  dest="cpu", type="long", default=1, help="Number of cpus to use. Default %default")
parser.add_option("--blocks",  dest="blocks", type="long", default=500, help="number of blocks. Default %default")
parser.add_option("--walltime",  dest="walltime", type="string", default="0:59:50", help="Max time for job run. Default %default")
parser.add_option("--chunk",  dest="chunk", type="long",  default=200000, help="Chunk size. Default %default")
parser.add_option("--maxchunk",  dest="maxchunk", type="long",  default=2e6, help="Maximum number of chunks. Default %default")
parser.add_option("--version",  dest="version", type="string",  default="", help="nametag to append to output file.")
parser.add_option("--no_parsl",  dest="no_parsl", action="store_true",  default=False, help="Run without parsl. Default: False")


(opt, args) = parser.parse_args()

samples = opt.samples

if len(args) < 1:
    if 'COFFEADATA' not in os.environ:
        print("ERROR: Enviroment variables not set. Run setup.sh first!")
        sys.exit()
    if samples == 'data': 
        file_name = os.path.join(os.environ['COFFEADATA'],'SingleMuon2017RunC.txt')
        fout_name = 'data_{}.root'.format(opt.version)
    elif samples == 'mc': 
        file_name = os.path.join(os.environ['COFFEADATA'],'Zj2017.txt') # PF cand info
        fout_name = 'mc_{}.root'.format(opt.version)
    elif samples == 'gc':
        fout_name = 'out.root'

    else:
        sys.error("ERROR: You must specify what kind of dataset you want to run [--samples]")
    
    print('Loading sets from file')    
    files = []
    with open(os.path.join(file_name),'r') as fread:
        files = fread.readlines()
        files = [x.strip() for x in files] 
        idx = np.arange(len(files))
        np.random.shuffle(idx)
        files = np.array(files)[idx]
                    
else:
    files = args
    fout_name = 'out.root'

fileset={}
for f in files:
    name = GetSamplename(f,opt.base_folder)
    if name in fileset:
        fileset[name].append(f)
    else:
        fileset[name] = [f]

ismc = False
for sample in fileset:
    print('Files: {0}, sample name: {1}'.format(fileset[sample],sample))
    if 'data' not in sample:
        ismc = True


nproc = opt.nproc
sched_options = '''
##SBATCH --account=gpu_gres 
##SBATCH --partition=gpu    
#SBATCH --cpus-per-task=%d
#SBATCH --mem=%d
##SBATCH --exclude=t3wn56
''' % (opt.cpu,opt.mem) 

x509_proxy = '.x509up_u%s'%(os.getuid())
wrk_init = '''
export XRD_RUNFORKHANDLER=1
export X509_USER_PROXY=/t3home/%s/%s
'''%(os.environ['USER'],x509_proxy)



if opt.no_parsl:
    output = processor.run_uproot_job(fileset,
                                      treename='Events',
                                      processor_instance=SignalProcessor(),
                                      executor=processor.futures_executor,
                                      executor_args={'workers':opt.nproc},
                                      maxchunks =opt.maxchunk,
                                      chunksize = opt.chunk,
                                      
    )


else:
    print("id: ",os.getuid())
    print(wrk_init)
    #sched_opts = ''
    #wrk_init = ''


    slurm_htex = Config(
        executors=[
            HighThroughputExecutor(
                label="coffea_parsl_slurm",
                #worker_debug=True,
                address=address_by_hostname(),
                prefetch_capacity=0,  
                heartbeat_threshold=60,
                cores_per_worker=1,
                #cores_per_worker=opt.cpu,
                max_workers=opt.cpu,
                provider=SlurmProvider(
                    channel=LocalChannel(),
                    #launcher=SrunLauncher(),
                    init_blocks=opt.blocks,
                    min_blocks = opt.blocks-20,                     
                    max_blocks=opt.blocks+20,
                    exclusive  = False,
                    parallelism=1,
                    nodes_per_block=1,
                    #cores_per_node = opt.cpu,
                    partition=opt.queue,
                    scheduler_options=sched_options,   # Enter scheduler_opt if needed
                    worker_init=wrk_init,         # Enter worker_init if needed
                    walltime=opt.walltime
                ),
            )
        ],
        initialize_logging=False,
        app_cache = True,
        retries=5,
        strategy=None,
    )

    dfk = parsl.load(slurm_htex)




    output = processor.run_uproot_job(fileset,
                                      treename='Events',
                                      processor_instance=SignalProcessor(),
                                      executor=processor.parsl_executor,
                                      executor_args={'config':None, 'retries':5,'tailtimeout':60,'xrootdtimeout':60},
                                      chunksize = opt.chunk
    )

for flow in output['cutflow']:
    print(flow, output['cutflow'][flow])

output = cutils.ScaleH(output,fileset)

    
if samples != 'gc':
    if not os.path.exists(os.path.join(os.environ['COFFEAHOME'],"pods")):
        os.makedirs(os.path.join(os.environ['COFFEAHOME'],"pods"))

    fout = uproot.recreate(os.path.join(os.environ['COFFEAHOME'],"pods",fout_name))
else:    
    fout = uproot.recreate(fout_name)

for var in output:
    if var == 'cutflow':continue
    if 'TTree' in var:
        branch_dict = {}
        for branch in output[var]:
            branch_dict[branch] =  "float32"
            
        fout['Events'] = uproot.newtree(branch_dict)
        fout['Events'].extend(output[var])
    else:
        for dataset in fileset:
            if len(output[var][dataset].sum("dataset").values()) < 1: continue #Don't try to save an empty histogram
            fout["{0}_{1}".format(dataset,var)] = hist.export1d(output[var][dataset].sum("dataset"))
fout.close()

if not opt.no_parsl:
    parsl.dfk().cleanup()
    parsl.clear()
