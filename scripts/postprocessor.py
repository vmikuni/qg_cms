import ROOT
from optparse import OptionParser
import utils
#from utils import *
import sys
import re
import h5py as h5
import os
import numpy as np
import copy
ROOT.gROOT.SetBatch(True)

parser = OptionParser(usage="%prog [opt]")
parser.add_option("-p","--plots",dest="plots", type="string", default="../plots", help="Path to store plots. [default: %default]")
parser.add_option("--name", type="string", default="test", help="basename of the resulting files. [default: %default]")
parser.add_option("-d","--dir", type="string", default="../pods", help="Base path for the folder with input root files. [default: %default]")
parser.add_option("--skip_add",action="store_true", default=False, help="Skip the hadd and only create histograms. [default: %default]")

(options, args) = parser.parse_args()

sample_list = [
    'SingleMuon',
    'DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8',
    #'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8',
]

plot_var = {
    'QGL':(30,0,1), 
    'DeepFlavQG':(30,0,1), 
    'njets':(8,1,9),
    'Jet_pt':(30,20,1000),
    'Jet_eta':(30,-2,2),
    'Dimuon_mass':(30,60,120),
    'Dimuon_pt':(30,10,500),
    'Asymmetry_frac':(30,0,0.4),
    'Delta_Phi':(30,-3.2,3.2),
    'ABCNet_up':(30,0,1),
    'ABCNet_down':(30,0,1),
    'ABCNet_strange':(30,0,1),
    'ABCNet_gluon':(30,0,1),
}


lumi = utils.lumi

save_dir = os.path.join(options.dir,options.name)
if not os.path.exists(save_dir):
    os.makedirs(save_dir)
    

if not options.skip_add:
    for sample in sample_list:
        path = os.path.join(options.dir,sample)
        hadd_txt = 'hadd -f {}/{}.root '.format(save_dir,sample)
        for root, dirs, fs in os.walk(path):
            for f in fs:
                if f.endswith(".root"):
                    hadd_txt += ' {}'.format(os.path.join(root,f))
        os.system(hadd_txt)
        rm_command = 'rm -r {}'.format(path)
        
        #os.system(rm_command)



histos = []
#
for sample in sample_list:
    fin = ROOT.TFile(os.path.join(save_dir,'{}.root'.format(sample)),"READ")
    tree= fin.Get('Events')
    for var in plot_var:
        if 'data' in utils.samples[sample]['name']:
            selection='(Jet_pt<{})&&(Jet_pt>{})&&(abs(Jet_eta)<{})'.format(utils.pt_thresh_high,utils.pt_thresh_low,utils.eta_thresh)
            tree.Draw(var+">>{}_{}{}".format(utils.samples[sample]['name'],var,plot_var[var]),selection)
            histos.append(copy.deepcopy(ROOT.gDirectory.Get("{}_{}".format(utils.samples[sample]['name'],var))))
        else:
            for flavour in utils.split_flavours:
                selection = "weight*({})".format(utils.split_flavours[flavour])         
                tree.Draw(var+">>{}_{}_{}{}".format(utils.samples[sample]['name'],flavour,var,plot_var[var]),selection)
                histos.append(copy.deepcopy(ROOT.gDirectory.Get("{}_{}_{}".format(utils.samples[sample]['name'],flavour,var))))

                histos[-1].Scale(lumi*utils.samples[sample]['xsec']/utils.samples[sample]['ngen'])
            
fout = ROOT.TFile(os.path.join(save_dir,'qg_histograms.root'),"RECREATE")
for hist in histos:
    hist.Write()
fout.Close()
    


