from collections import OrderedDict
#import ROOT as rt

#rt.PyConfig.IgnoreCommandLineOptions = True

lumi =  41.53e3

samples = {
    'DYJetsToLL_M-50_TuneCP5_13TeV-madgraphMLM-pythia8':
    {
        'name': 'DY_mad',
        'xsec': 6529.0,
        'ngen': 107044557.0,
    },
    
    'DYJetsToLL_M-50_TuneCP5_13TeV-amcatnloFXFX-pythia8':
    {
        'name': 'DY_amc',
        'xsec': 6529.0,
        'ngen': 3579807739609.9185,
    },
    


    'QCD_HT200to300_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8':
    {
        'name': 'QCD_200',
        'xsec': 1710000.0,
        #'ngen': 29438093.11328125,
        'ngen': 29438093.11328125,

    },

    'QCD_HT300to500_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8':
    {
        'name': 'QCD_300',
        'xsec': 347500.0,
        'ngen': 42933997.02734375,
        

    },

    'QCD_Pt_30to50_TuneCP5_13TeV_pythia8':
    {
        #'name': 'QCD_pt_30',
        'name': 'QCD_pt',
        'xsec': 1.069e+08,
    },
    'QCD_Pt_50to80_TuneCP5_13TeV_pythia8':
    {
        #'name': 'QCD_pt_50',
        'name': 'QCD_pt',
        'xsec': 1.571e+07,
    },
    'QCD_Pt_80to120_TuneCP5_13TeV_pythia8':
    {
        #'name': 'QCD_pt_80',
        'name': 'QCD_pt',
        'xsec': 2.338e+06,
    },
    'QCD_Pt_120to170_TuneCP5_13TeV_pythia8':
    {
        #'name': 'QCD_pt_120',
        'name': 'QCD_pt',
        'xsec': 4.074e+05,
    },
    'QCD_Pt_170to300_TuneCP5_13TeV_pythia8':
    {
        'name': 'QCD_pt',
        #'name': 'QCD_pt_170',
        'xsec': 1.035e+05 ,
    },
    'QCD_Pt_300to470_TuneCP5_13TeV_pythia8':
    {
        #'name': 'QCD_pt_300',
        'name': 'QCD_pt',
        'xsec': 7.760e+03,
    },
    'QCD_Pt_470to600_TuneCP5_13TeV_pythia8':
    {
        #'name': 'QCD_pt_470',
        'name': 'QCD_pt',
        'xsec': 5.516e+02,
    },
    'QCD_Pt_600to800_TuneCP5_13TeV_pythia8':
    {
        #'name': 'QCD_pt_600',
        'name': 'QCD_pt',
        'xsec': 1.563e+02,
    },
    'QCD_Pt_800to1000_TuneCP5_13TeV_pythia8':
    {
        #'name': 'QCD_pt_800',
        'name': 'QCD_pt',
        'xsec': 2.624e+01 ,
    },

    


    'QCD_HT500to700_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8':
    {
        'name': 'QCD_500',
        'xsec': 32060.0,
        'ngen': 40803303.1953125,

    },

    'QCD_HT700to1000_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8':
    {
        'name': 'QCD_700',
        'xsec': 6829.0,
        'ngen': 32686440.44140625,

    },


    'QCD_HT1000to1500_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8':
    {
        'name': 'QCD_1000',
        'xsec': 1207.0,
        'ngen': 13472786.80078125,

    },

    'QCD_HT1500to2000_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8':
    {
        'name': 'QCD_1500',
        'xsec': 120.0,
        'ngen': 11838680.60546875,

    },

    'QCD_HT2000toInf_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8':
    {
        'name': 'QCD_2000',
        'xsec': 25.25,
        'ngen': 6019013.12109375,

    },
    
    'SingleMuon': {'name': 'data_SingleMuon'},
    'Run2017E': {'name': 'data_SingleMuon'},
    'Run2017D': {'name': 'data_SingleMuon'},
    'Run2017C': {'name': 'data_SingleMuon'},
    'Run2017B': {'name': 'data_SingleMuon'},
    'Run2017G': {'name': 'data_SingleMuon'},
    'Run2017B': {'name': 'data_SingleMuon'},
    'Run2017F': {'name': 'data_SingleMuon'},
    'Run2017H': {'name': 'data_SingleMuon'},


}


pt_thresh_low=30.0
pt_thresh_high=40
eta_thresh=2.4

split_flavours = {
    #'Quark':'(abs(Jet_partonFlavour)!=21)&&(Jet_pt<{})&&(Jet_pt>{})&&(abs(Jet_eta)<{})'.format(pt_thresh_high,pt_thresh_low,eta_thresh),
    
    'Up':'(abs(Jet_partonFlavour)==2)&&(Jet_pt<{})&&(Jet_pt>{})&&(abs(Jet_eta)<{})'.format(pt_thresh_high,pt_thresh_low,eta_thresh),
    'Down':'(abs(Jet_partonFlavour)==1)&&(Jet_pt<{})&&(Jet_pt>{})&&(abs(Jet_eta)<{})'.format(pt_thresh_high,pt_thresh_low,eta_thresh),
    'Strange':'(abs(Jet_partonFlavour)==3)&&(Jet_pt<{})&&(Jet_pt>{})&&(abs(Jet_eta)<{})'.format(pt_thresh_high,pt_thresh_low,eta_thresh),
    'Gluon':'(abs(Jet_partonFlavour)==21)&&(Jet_pt<{})&&(Jet_pt>{})&&(abs(Jet_eta)<{})'.format(pt_thresh_high,pt_thresh_low,eta_thresh),
    'Others':'(abs(Jet_partonFlavour)!=2)&&(abs(Jet_partonFlavour)!=1)&&(abs(Jet_partonFlavour)!=3)&&(abs(Jet_partonFlavour)!=21)&&(Jet_pt<{})&&(Jet_pt>{})&&(abs(Jet_eta)<{})'.format(pt_thresh_high,pt_thresh_low,eta_thresh),
}

processlist =OrderedDict([
    ('data',[['data_SingleMuon'], '#000000']),
    
    # ('Z+Up',[['DY_amc_Up'],'#4daf4a']),
    # ('Z+Down',[['DY_amc_Down'],'#8856a7']),
    # ('Z+Strange',[['DY_amc_Strange'],'#377eb8']),
    # ('Z+Gluon',[['DY_amc_Gluon',],'#e41a1c']),
    # ('Z+Others',[['DY_amc_Others'],'#fed976']),
    # ('Z+Quarks',[['DY_amc_Quark'],'#8856a7']),

    ('Z+Up',[['DY_mad_Up'],'#4daf4a']),
    ('Z+Down',[['DY_mad_Down'],'#8856a7']),
    ('Z+Strange',[['DY_mad_Strange'],'#377eb8']),
    ('Z+Gluon',[['DY_mad_Gluon',],'#e41a1c']),
    ('Z+Quarks',[['DY_mad_Quark'],'#8856a7']),
    ('Z+Others',[['DY_mad_Others'],'#fed976']),


    ('QCD',[['QCD_200','QCD_300','QCD_500','QCD_700','QCD_1000','QCD_1500','QCD_2000',],'#ff7f00']),
])

def SetupBox(box_,yy_,fill = 0):
    import ROOT
    box_.SetLineStyle( ROOT.kSolid )
    box_.SetLineWidth( 1 )
    box_.SetLineColor( ROOT.kBlack )
    box_.SetFillColor(fill)


def SetupCanvas(c,logy):
    import Plotting_cfg as cfg 
    c.SetFillColor(0)
    c.SetBorderMode(0)
    c.SetFrameFillStyle(0)
    c.SetFrameBorderMode(0)
    c.SetLeftMargin( cfg.L/cfg.W_ref)
    c.SetRightMargin( cfg.R/cfg.W_ref)
    c.SetTopMargin( cfg.T/cfg.H )
    c.SetBottomMargin( cfg.B/cfg.H )
    c.SetTickx(0)
    c.SetTicky(0)
    if logy: c.SetLogy()


def AddOverflow(h):
    b0 = h.GetBinContent(0)
    e0 = h.GetBinError(0)
    nb = h.GetNbinsX()
    bn = h.GetBinContent(nb + 1)
    en = h.GetBinError(nb + 1)

    h.SetBinContent(0, 0)
    h.SetBinContent(nb+1, 0)
    h.SetBinError(0, 0)
    h.SetBinError(nb+1, 0)

    h.SetBinContent(1, h.GetBinContent(1) + b0)
    h.SetBinError(1, (h.GetBinError(1)**2 + e0**2)**0.5 )

    h.SetBinContent(nb, h.GetBinContent(nb) + bn)
    h.SetBinError(nb, (h.GetBinError(nb)**2 + en**2)**0.5 )


def StackPlot(hstack,fit,plotName,plotData=True,is_log=True):
    import copy
    import ROOT
    import Plotting_cfg as cfg
    herr = None
    c = ROOT.TCanvas(plotName,plotName,5,30,cfg.W_ref,cfg.H_ref)
    pad1 = ROOT.TPad("pad1", "pad1", 0.0, 0.2 if plotData else 0.0, 1, 1.0)
    pad1.Draw()
    pad1.cd()
    SetupCanvas(pad1, is_log)

    hframe = fit[list(fit.keys())[0]].Clone('frame')
    hframe.Reset()
    
    hframe.SetAxisRange(1, hstack.GetMaximum()*cfg.FRMLOG if is_log else hstack.GetMaximum()*cfg.FRML,"Y");
    xAxis = hframe.GetXaxis()
    xAxis.SetLabelSize(0.)
    xAxis.SetTitleSize(0.)
    
    yAxis = hframe.GetYaxis()
    yAxis.SetNdivisions(6,5,0)
    yAxis.SetLabelSize(cfg.FLS)
    yAxis.SetTitleSize(cfg.FTS)
    yAxis.SetTitleOffset(cfg.FTO)
    yAxis.SetMaxDigits(3)
    yAxis.SetTitle("Events / bin")
    hframe.Draw()
    hstack.Draw("histsame")
    c.Update()
    c.Modified()
    # print(hstack.GetNhists())
    # input()
    


    for hprocess in list(fit.keys()):
        if 'data' in hprocess: continue
        if herr == None:
            herr = fit[hprocess].Clone(plotName+'err')
        else:
            herr.Add(fit[hprocess].Clone(plotName+hprocess+'err'))


    herr.SetFillColor( ROOT.kBlack )
    herr.SetMarkerStyle(0)
    herr.SetFillStyle(3354)
    ROOT.gStyle.SetHatchesLineWidth(1)
    ROOT.gStyle.SetHatchesSpacing(1)
    if plotData:
        fit['data'].Draw("esamex0")

    herr.Draw('e2same')


    box = create_paves(lumi, "DataPAS", CMSposX=0.26, CMSposY=0.9,
                             prelimPosX=0.22, prelimPosY=0.85,
                             lumiPosX=0.967, lumiPosY=0.87, alignRight=False)
    box["lumi"].Draw("same")
    box["CMS"].Draw("same")
    box["label"].Draw()



    pad1.cd()
    pad1.Update()
    pad1.RedrawAxis()
    frame = c.GetFrame()
    
    latex = ROOT.TLatex()
    
    legend =  ROOT.TPad("legend_0","legend_0",cfg.x0_l + cfg.xshiftm,cfg.y0_l + cfg.yshiftm,cfg.x1_l+cfg.xshiftp, cfg.y1_l + cfg.yshiftp )
    
    legend.Draw()
    legend.cd()

    if plotData:
        gr_l =  ROOT.TGraphErrors(1, cfg.x_l, cfg.y_l, cfg.ex_l, cfg.ey_l)
        ROOT.gStyle.SetEndErrorSize(0)
        gr_l.SetMarkerSize(0.9)
        gr_l.Draw("0P")

    latex.SetTextFont(42)
    latex.SetTextAngle(0)
    latex.SetTextColor(ROOT.kBlack)
    latex.SetTextSize(0.18)
    latex.SetTextAlign(12)
    yy_ = cfg.y_l[0]
    if plotData:
        latex.DrawLatex(cfg.xx_+1.*cfg.bwx_,yy_,"Data")
    nkey=0

    for key in list(fit.keys()):
        if 'data' in key: continue
        box_ = ROOT.TBox()
        SetupBox(box_,yy_,ROOT.TColor.GetColor(processlist[key][1]))
        if nkey %2 == 0:
            xdist = cfg.xgap
        else:
            yy_ -= cfg.gap_
            xdist = 0
        box_.DrawBox( cfg.xx_-cfg.bwx_/2 - xdist, yy_-cfg.bwy_/2, cfg.xx_+cfg.bwx_/2 - xdist, yy_+cfg.bwy_/2 )
        box_.SetFillStyle(0)
        box_.DrawBox( cfg.xx_-cfg.bwx_/2 -xdist, yy_-cfg.bwy_/2, cfg.xx_+cfg.bwx_/2-xdist, yy_+cfg.bwy_/2 )
        latex.DrawLatex(cfg.xx_+1.*cfg.bwx_-xdist,yy_,key)
        nkey+=1


    box_ = ROOT.TBox()
    if nkey %2!=0:
        yy_ -= cfg.gap_
    c.Update()



    if plotData:
        c.cd()
        p1r = ROOT.TPad("p4","",0,0,1,0.26)
        
        p1r.SetRightMargin(cfg.P2RM)
        p1r.SetLeftMargin(cfg.P2LM)
        p1r.SetTopMargin(cfg.P2TM)
        p1r.SetBottomMargin(cfg.P2BM)
        p1r.SetTicks()
        p1r.Draw()
        p1r.cd()
        
        xmin = float(herr.GetXaxis().GetXmin())
        xmax = float(herr.GetXaxis().GetXmax())
        one = ROOT.TF1("one","1",xmin,xmax)
        one.SetLineColor(1)
        one.SetLineStyle(2)
        one.SetLineWidth(1)
        
        nxbins = herr.GetNbinsX()
        hratio = fit['data'].Clone()
        
        he = herr.Clone()
        he.SetFillColor( 16 )
        he.SetFillStyle( 1001 )
        
        for b in range(nxbins):
            nbkg = herr.GetBinContent(b+1)
            ebkg = herr.GetBinError(b+1)
            
            ndata = fit['data'].GetBinContent(b+1)
            edata = fit['data'].GetBinError(b+1)
            r = ndata / nbkg if nbkg>0 else 0
            rerr = edata / nbkg if nbkg>0 else 0
            
            hratio.SetBinContent(b+1, r)
            hratio.SetBinError(b+1,rerr)
            
            he.SetBinContent(b+1, 1)
            he.SetBinError(b+1, ebkg/nbkg if nbkg>0 else 0 )
            
        hratio.GetYaxis().SetRangeUser(cfg.RMIN,cfg.RMAX)

        hratio.SetTitle("")
        
        hratio.GetXaxis().SetTitle(plotName)
        hratio.GetXaxis().SetTitleSize(cfg.RTSX)
        hratio.GetXaxis().SetTitleOffset(cfg.RTOX)
        hratio.GetXaxis().SetLabelSize(cfg.RLSX)
        #hratio.GetXaxis().SetTickLength(0.09)
        
        
        #for b in range(hratio.GetNbinsX()):
        #    hratio.GetXaxis().SetBinLabel(b+1, str(int(hratio.GetBinLowEdge(b+1))) )
        
        hratio.GetXaxis().SetLabelOffset(0.02)
        
        hratio.GetYaxis().SetTitleSize(cfg.RTSY)
        hratio.GetYaxis().SetLabelSize(cfg.RLSY)
        hratio.GetYaxis().SetTitleOffset(cfg.RTOY)
        hratio.GetYaxis().SetTitle("      Data/Sim.")
        hratio.GetYaxis().SetDecimals(1)
        hratio.GetYaxis().SetNdivisions(2,2,0) #was 402
        #hratio.GetXaxis().SetNdivisions(6,5,0)
        
        hratio.Draw("pe")
        #    setex2.Draw()
        he.Draw("e2same")
        one.Draw("SAME")
        #turn off horizontal error bars
        #    setex1.Draw()
        #hratio.Draw("PEsame")
        hratio.Draw("PE0X0same")
        hratio.Draw("sameaxis") #redraws the axes
        p1r.Update()
            #raw_input("")


    return copy.deepcopy(c)

def create_paves(lumi, label, CMSposX=0.11, CMSposY=0.9, prelimPosX=0.11, prelimPosY=0.85, lumiPosX=0.95, lumiPosY=0.951, alignRight=False, CMSsize=0.75*0.08, prelimSize=0.75*0.08*0.76, lumiSize=0.6*0.08):
    import ROOT
    import Plotting_cfg  as cfg
    #pt_lumi = ROOT.TPaveText(xhi-0.25, ylo, xhi, yhi,"brNDC")
    pt_lumi = ROOT.TPaveText(lumiPosX-0.25, lumiPosY, lumiPosX, 1.0,"brNDC")
    pt_lumi.SetFillStyle(0)
    pt_lumi.SetBorderSize(0)
    pt_lumi.SetFillColor(0)
    pt_lumi.SetTextFont(42)
    pt_lumi.SetTextSize(lumiSize)
    pt_lumi.SetTextAlign(31) #left=10, bottom=1, centre=2
    pt_lumi.AddText( "{0:.1f}".format(lumi/1e3)+" fb^{-1} (13 TeV)" )


    # if CMSpos == 0: #outside frame
    #     pt_CMS = ROOT.TPaveText(xlo, ylo, xlo+0.1, yhi,"brNDC")
    # elif CMSpos == 1: #left
    #     pt_CMS = ROOT.TPaveText(xlo+0.04, ylo-0.09, xlo+0.14, ylo-0.04,"brNDC")
    # elif CMSpos == 2: #center
    #     pt_CMS = ROOT.TPaveText(xlo+0.4, ylo-0.09, xlo+0.5, ylo-0.04,"brNDC")
    # elif CMSpos == 3: #right
    #     pt_CMS = ROOT.TPaveText(xhi-0.2, ylo-0.09, xhi-0.1, ylo-0.04,"brNDC")
    if alignRight:
        pt_CMS = ROOT.TPaveText(CMSposX-0.1, CMSposY, CMSposX, CMSposY+0.05,"brNDC")
    else:
        pt_CMS = ROOT.TPaveText(CMSposX, CMSposY, CMSposX+0.1, CMSposY+0.05,"brNDC")
    pt_CMS.SetFillStyle(0)
    pt_CMS.SetBorderSize(0)
    pt_CMS.SetFillColor(0)
    pt_CMS.SetTextFont(61)
    pt_CMS.SetTextSize(CMSsize)
    #pt_CMS.SetTextAlign(31 if CMSpos==3 else 11)
    pt_CMS.SetTextAlign(31 if alignRight else 11 )
    pt_CMS.AddText("CMS")

    # if PrelimPos == 0: #outside frame
    #     pt_prelim = ROOT.TPaveText(xlo+0.09, ylo, xlo+0.3, yhi,"brNDC")
    # elif PrelimPos == 1: #left beside CMS
    #     pt_prelim = ROOT.TPaveText(xlo+0.13, ylo-0.09, xlo+0.34, ylo-0.04,"brNDC")
    # elif PrelimPos == 2: #left under CMS
    #     pt_prelim = ROOT.TPaveText(xlo+0.04, ylo-0.15, xlo+0.14, ylo-0.10,"brNDC")
    # elif PrelimPos == 3: #right under CMS
    #     pt_prelim = ROOT.TPaveText(xhi-0.2, ylo-0.15, xhi-0.1, ylo-0.10,"brNDC")
    if alignRight:
        pt_prelim = ROOT.TPaveText(prelimPosX-0.2, prelimPosY, prelimPosX, prelimPosY+0.05,"brNDC")
    else:
        pt_prelim = ROOT.TPaveText(prelimPosX, prelimPosY, prelimPosX+0.2, prelimPosY+0.05,"brNDC")
    pt_prelim.SetFillStyle(0)
    pt_prelim.SetBorderSize(0)
    pt_prelim.SetFillColor(0)
    pt_prelim.SetTextFont(52)
    pt_prelim.SetTextSize(prelimSize)
    #pt_prelim.SetTextAlign(31 if PrelimPos==3 else 11)
    pt_prelim.SetTextAlign(31 if alignRight else 11 )
    if label == "SimPAS":
        pt_prelim.AddText("Simulation Preliminary")
    elif label == "DataPAS":
        pt_prelim.AddText("Preliminary")
    elif label == "Sim":
        pt_prelim.AddText("Simulation")
    elif label == "Data":
        pt_prelim.AddText("")
    elif label == "SimSupp":
        pt_prelim.AddText("Simulation Supplementary")
    elif label == "DataSupp":
        pt_prelim.AddText("Supplementary")
    elif label == "Int":
        pt_prelim.AddText("Internal")

    return {"lumi":pt_lumi, "CMS":pt_CMS, "label":pt_prelim}
