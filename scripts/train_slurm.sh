#!/bin/bash
#
#SBATCH --job-name=train_job 
#SBATCH --account=gpu_gres               # to access gpu resources
#SBATCH --partition=gpu                                           
#SBATCH --nodes=1                        # request to run job on single node                                       
##SBATCH --ntasks=4                     # request 10 CPU's (t3gpu01/02: balance between CPU and GPU : 5CPU/1GPU)      
#SBATCH --gres=gpu:1                     # request  for two GPU's on machine, this is total  amount of GPUs for job        
#SBATCH --mem=20000                     # memory (per node)
#SBATCH --time=0-50:30                   # time  in format DD-HH:MM
#SBATCH -e slurm/slurm-gpu-%A.err
#SBATCH -o slurm/slurm-gpu-%A.out

# Slurm reserves two GPU's (according to requirement above), those ones that are recorded in shell variable CUDA_VISIBLE_DEVICES
echo CUDA_VISIBLE_DEVICES : $CUDA_VISIBLE_DEVICES
echo "--------------------------------------------------------------------"
env
echo "--------------------------------------------------------------------"
cd $COFFEAHOME/scripts
source activate QGGPU
pyenv versions
echo "--------------------------------------------------------------------"
nvidia-smi
echo "--------------------------------------------------------------------"

#python train.py --num_point 100  --nfeat 13 --log_dir qg_cms --params [10,1,32,128,128,128,2,64,128,128,128,128,256]  --batch_size 64 --min loss --wd 0.01 --decay_step 200000
#python train.py --num_point 100  --nfeat 13 --log_dir qg_cms_v2 --params [10,1,32,128,128,128,2,64,128,128,128,128,256]  --batch_size 64 --min loss --wd 0.01 --decay_step 200000
python train.py --num_point 100  --nfeat 13 --log_dir qg_cms_v3 --params [10,1,32,128,128,128,2,64,128,128,128,128,256]  --batch_size 64 --min loss --wd 0.01 --decay_step 200000
#python train.py --num_point 100  --nfeat 13 --log_dir btag_cms --params [20,1,32,128,128,128,2,64,128,128,128,128,256] --batch_size 64 --min loss --wd 0.01 --decay_step 200000
