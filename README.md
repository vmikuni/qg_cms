# Quark gluon discrimination for CMS

This repo uses ```coffea``` to read and prepare the data that can be further used to other ML methods.

# Installation

The installation follows the helpful guide from Korbinian (https://github.com/kschweiger/TF4ttHFH) to install all the required packages on ```tier3``` by setting up a virtual environment with pyenv.

```bash
curl https://pyenv.run | bash
```

After the setup the following needs to be added to the file loaded on loging (`.bash_profile` etc.)

```bash
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
    eval "$(pyenv virtualenv-init -)"
fi
```

To setup the required packages do 

```bash
source init.sh
source setup.sh
``` 

The first command takes a while to finish. The next time you only need to use:

```bash
source setup.sh
```

To properly set the environment variables.

# Analyzing the data

To verify that everything was properly setup you can try to run the code on a single file by doing

```bash
cd scripts
python analysis_QG.py --version test --samples train --save_h5 --chunk 5000 --maxchunk 2 --no_parsl root://t3dcachedb03.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/algomez/NanoAODJMAR/NanoAODv6/QCD_Pt_600to800_TuneCP5_13TeV_pythia8/QCDPt600to800TuneCP513TeVpythia8RunIIFall17MiniAODv2-PU201712Apr201894Xmc2017/200413_085218/0000/nano102x_on_mini94x_2017_mc_NANO_7.root
```

Do not forget to have a valid **grid certificate** to be able to access the files in the  storage element.
To test if the ```parsl``` package for job management was set up properly you can run the same command without the ```--no_parsl``` flag.

If everything worked fine, a root file will be created in the folder ```pods``` containing some histograms. An ```h5``` file should also have been created inside the folder ```h5``` to be used for machine learning (ML) purposes.
**Important:** This setup does not contain root installed by default, since it was made to be root independent. 
To train using more files use:
```bash
python analysis_QG.py --version test --samples [train/test/eval] --save_h5 --chunk 5000
```
The required flags are:
* --samples: Define wich set of files to load (train/test/eval). The list of files loaded are listed in the ```.txt``` files under the data directory.
* --version: string of text to be appended to the output files.
* --save_h5: boolean flag to whether to create h5 files.

More options can be accessed by running the code with ```-h```.


# Training the classifier with ABCNet

After a set of training, testing and evaluation files are created you are almost ready to start the ML training. First, you need to create a set of ```.txt``` files under the h5 folder, to choose the files to be used. Edit the files ```train_files.txt```, ```test_files.txt```, and ```evaluate_files.txt``` to list the correct files that you just created.

```bash
python train.py --num_point 100 --nfeat 13 --nglob 2 --log_dir qg_test --batch_size 64 --min loss 
```

The required flags are:

* --num_point: Number of points in the point cloud.
* --nfeat: Number of features per particle.
* --nglob: Number of global features per event.

More options can be accessed by running the code with ```-h```.

To evaluate the performance using the evaluation set just run:

```bash
python evaluate.py --num_point 100  --nfeat 13 --model_path ../logs/qg_test --batch 1000 --name qg_test --nglob 2 --modeln 0
```

The required options are similar to the training options. The additional requirements are:

* --model_path: path to the trained model to evaluate.
* --name: string to be added to the output h5 file
* --modeln: Number of the epoch to be loaded. 

For large training files, you can use the GPUs available at tier 3 for the training. To perform the training on GPUs you can run

```bash
sbatch train_slurm.sh
```
as an example.

# Data and MC comparison

To compare the trained results with the UL2017 data, we are going to use a sample containing Z+jets. The main code to analyze events, apply cuts and evaluate the results is called ```analysis_data.py```. To test the code run with:

```bash
python analysis_data.py  --maxchunk 2 --chunk 10000 --no_parsl /pnfs/psi.ch/cms/trivcat/store/user/vmikuni/UL2017_QG/SingleMuon/job_606_out.root
```
To run the full dataset, a second option is to use ```gridcontrol``` which serves the same purpose as parsl: run the code in parallel using the batch system at tier3. To select the samples to be processed, edit the file ```run_qg.conf``` under the ```gc``` folder. 

When the selected samples are ready to go, you can first test the job submission by uncommenting the flag ```jobs = 1``` inside the ```.conf``` file.
To run the submission use:
```bash
cd ${COFFEAHOME}/scripts/gc/
/t3home/vmikuni/grid-control/go.py run_qg.conf
```
The output root files should be stored in the pods directory. 

# Plotting

Plotting scripts to use the root files as inputs are also stored in the scripts folder. First, we have to add ROOT in order to load histograms and plot the quantities. To do so, load the file:

```bash
cd ${COFFEAHOME}
source setup_root.sh
```

Since the second load might interfere with the paths of the setup, it's advisable to load it ```only``` when you want to make the plots. To add the root files in the output and prepare histograms, use the script ```postprocessor.py```. Edit this script in case you want to change the samples, binning, or variables to be saved. WHen you are happy with it, just run:

```bash
cd ${COFFEAHOME}/scripts
python postprocessor.py --version [OUTPUTFOLDERNAME] [--skip_add]
```
Add the flag ```--skip_add``` in case you just want to change the binning/list of distributions after you already merged the files.
Now, to compare the MC and data, use:

```bash
python Plot.py --name [OUTPUTFOLDERNAME] --plotData
```

Where you can also change the distributions to be plotted and which processes are going to be shown. All the results are stored under the plots folder.

